<?php
	//Métodos uteis que podem/devem ser utilizados em multiplos arquivos .php
	//Necessita de include $_SERVER['DOCUMENT_ROOT'] . '/utilities.php'
	
	  // A sessão precisa ser iniciada em cada página diferente
	if (!isset($_SESSION)) {
		session_start();
	}
	
	//escapa caracteres especias, exceto %, para ser utilizado ao receber valores de input
	function escape_all_specials($inp) { 
		if(is_array($inp)) 
			return array_map(__METHOD__, $inp); 

		if(!empty($inp) && is_string($inp)) { 
			return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
		} 
		return $inp;			
	} 
	
	function Mask($mask,$str){
		$str = str_replace(" ","",$str);

		for($i = 0; $i < strlen($str); $i++){
		  $mask[strpos($mask,"#")] = $str[$i];
		}

		return $mask;
	}
	//usado para transformar dados nullos em valores padrão
	function nvl($val, $val2){
		if($val == null){
			return $val2;
		} else {
			return $val;
		}
	}
  
  
  //SCRIPTS TALVEZ UTEIS!!
  
//				echo "<script language='javascript'>alert('Login e/ou senha incorreta!');</script>"; // Prompts the user
//				echo "<script language='javascript'>window.location.assign('login.php');</script>"; // redirects to login.php	


?>