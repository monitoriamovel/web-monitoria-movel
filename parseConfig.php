<?php

	require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php'; // include Composer's autoloader
	
	use Parse\ParseClient;
	
	$app_id = 'monitoriamovel';
	$master_key = 'monitoria_movel_furb';
	ParseClient::initialize( $app_id, null, $master_key );

	ParseClient::setServerURL('http://monitoriamovel.herokuapp.com','parse');

	
	//TESTES	
/*
	$pai = new ParseObject("pai");
	$pai->set("nome","Jose");
	$pai->set("funcao","testar One-to-One/Many");
	
	$filho = new ParseObject("filho");
	$filho->set("nome","Miguel");
	$filho->set("funcao","testar Many-to-Many");
	
	$mae = new ParseObject("mae");
	$mae->set("nome","Joana");
	$mae->set("funcao","testar Many-to-Many");
	
	$memorias = $filho->getRelation("memorias");
	$memorias->add($mae);
*/	
//	echo nl2br("\n pai... " . $pai->get("nome") . "..." . $pai->get("funcao"));
//	echo nl2br("\n filho... " . $filho->get("nome") . "..." . $filho->get("funcao"));
//	echo nl2br("\n mae... " . $mae->get("nome") . "..." . $mae->get("funcao"));
//	echo nl2br("\n\n");
	
/*	// Signup

	$user = new ParseUser();
	$user->set("username", "my name");
	$user->set("password", "my pass");

	// other fields can be set just like with ParseObject
	$user->set("phone", "415-392-0202");

	try {
	  $user->signUp();
	  // Hooray! Let them use the app now.
	  $user = $query->get("lFBxjzxfxh"); //id on mongo db
		  echo $user->get("usuario") . ', ' 
		  . $user->get('nome') . ', ' 
		  . $user->get('senha') . ', ' 
		  . $user->getCreatedAt();
		  echo 'Success!';
	} catch (ParseException $ex) {
	  // Show the error message somewhere and let the user try again.
	  echo "Error: " . $ex->getCode() . " " . $ex->getMessage();
	}
*/

/*
	// Login
	try {
		$user = ParseUser::logIn("foo", "Q2w#4!o)df");
	} catch(ParseException $ex) {
	echo 'error in' . $ex->getMessage();
	}

	
	$user->setUsername("foo");
	$user->setPassword("Q2w#4!o)df");
	try {
		$user->signUp();
	} catch (ParseException $ex) {
		echo 'error in' . $ex->getMessage();
	}
*/	

?>