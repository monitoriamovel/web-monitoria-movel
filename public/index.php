<?php
    $nome_pag = "Login";
	include 'header.php';
	
	if(isset($_SESSION['user'])){
		header("location: monitores");
	}

	if(isset($_SESSION['err'])){
		echo "<script language='javascript'>alert('Login e/ou senha incorreta!');</script>"; // Prompts the user
		$_SESSION['err'] = null;
	}
?>
	<link href="src/css/index.css" rel="stylesheet">	
    <!-- Index page specific css 
	    <link href="src/css/style.css" rel="stylesheet">
	blue_1 = R0 G95 B164 = 005FA4
	blue_2 = R0 G84 B154 = 00549A
	yellow_1 = R254 G232 B0 = FEE800
	yellow_2 = R249 G249 B241
	
	-->
	
    <div class="container">

      <form class="form-signin" action="checkLoginUser" method="post">
        <h2 class="form-signin-heading"><img id="logo" src="src/img/logo_2.gif"></h2>
        <input class="form-control" autofocus="" required="true" type="text" placeholder="Usuário" name="username">
        <input class="form-control" required="true" type="password" placeholder="Senha" name="password">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Logar</button>
      </form>

    </div>

<?php
	include 'footer.php';
?>