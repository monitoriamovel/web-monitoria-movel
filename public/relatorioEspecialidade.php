<?php
	$nome_pag = "Relatorios Especialidades";
	include 'header.php';

	if(!isset($_SESSION['user'])){
		header("location: index");
	} else {
		$user = $_SESSION['user'];
	}
	
	use Parse\ParseObject;
	use Parse\ParseQuery;
	use Parse\ParseACL;
	use Parse\ParsePush;
	use Parse\ParseUser;
	use Parse\ParseInstallation;
	use Parse\ParseException;
	use Parse\ParseAnalytics;
	use Parse\ParseFile;
	use Parse\ParseCloud;
	use Parse\ParseClient;
		
	try {
		$chart = new ParseObject("chart");

		if(isset($_GET['tipoBusca'])) {
			$chart->set("tipoBusca",$_GET['tipoBusca']);
		} else {
			$chart->set("tipoBusca",1);
		}
		if(isset($_GET['dtInicio']) && trim($_GET['dtInicio']) != '') {
			$chart->set("dtInicio",$_GET['dtInicio']);
		} else {
			if(date("m")-2 < 1){
				$mes = (date("m")-2+12); 
				$chart->set("dtInicio", date("Y")-1 . '-' . ($mes < 10 ? '0'.$mes : $mes) . '-' . date("d"));
				echo $mes . '<br/>' . $chart->get("dtInicio") . '<br/>';
			} else {
				$mes = (date("m")-2);
				$chart->set("dtInicio", date("Y") . '-' . ($mes < 10 ? '0'.$mes : $mes) . '-' . date("d"));
			}
		}
		if(isset($_GET['dtFim']) && trim($_GET['dtFim']) != '') {
			$chart->set("dtFim",$_GET['dtFim']);
		} else{
			$chart->set("dtFim",date("Y-m-d"));			
		}		
					
		$dtInicio = new DateTime($chart->get("dtInicio")); 
		$dtFim = new DateTime($chart->get("dtFim")); 

		if($dtInicio > $dtFim){
			$dt = $dtInicio;
			$dtInicio = $dtFim;
			$dtFim = $dt;
			
			$dt = $chart->get("dtInicio");
			$chart->set("dtInicio",$chart->get("dtFim"));
			$chart->set("","");
		}

		$dtInicio->setTime(0,0,0);
		$dtFim->setTime(23,59,59);
		
		$iniMes = $dtInicio->format('m')+0;
		$iniAno = $dtInicio->format('Y')+0;
		$endMes = $dtFim->format('m')+1;
		$endAno = $dtFim->format('Y')+0;

		if($endMes > 12){
			$endMes = 1;
			$endAno = $endAno+1;
		}
		
		$data[] = array();
		$qtd = 0;
		
		while(!($iniMes == $endMes && $iniAno == $endAno)){
			$query = new ParseQuery("atendimento");
			
			$dtIniBusca = new DateTime(); 
			$dtIniBusca->setDate($iniAno, $iniMes, 1);
			$dtIniBusca->setTime(0,0,0);
			
			$endMonth = $iniMes+1;
			$endYear = $iniAno;
			if($endMonth > 12){
				$endMonth = 1;
				$endYear = $endYear + 1;
			}
			$dtFimBusca = new DateTime();
			$dtFimBusca->setDate($endYear, $endMonth, 1);
			$dtFimBusca->setTime(0,0,0);
			
			$query->greaterThanOrEqualTo('createdAt',$dtIniBusca);
			$query->lessThan('createdAt',$dtFimBusca);
			
			$query->greaterThanOrEqualTo('avaliacao',0);
			
			$resultPer = $query->find();
			$qtdAtend = 0;
			$qtdAtendMes = 0;
			$maiorId = 'Não informado';

			$esp = array();
			for($x=0; $x < count($resultPer); $x++){
				$monitor = $resultPer[$x]->get("monitor");
				$monitor->fetch();
				if(($chart->get('tipoBusca') == 2) or ($monitor->get('verificado') == $chart->get('tipoBusca')) ){
					$especi2 = explode(',', $monitor->get("especialidades"));
					$qtdAtendMes++;
					for($y=0; $y < count($especi2); $y++){
						$string = trim($especi2[$y]);
						$esp[$string] = (isset($esp[$string]) ? $esp[$string] : 0) + 1;
						
						if($esp[$string] > $qtdAtend){
							$qtdAtend = $esp[$string];
							$maiorId = $string;
						}
					}
				}
			}
			
			$data[$qtd] = array($iniMes, $iniAno, $qtdAtend, $maiorId, $qtdAtendMes);			
			$qtd = $qtd+1;
			
			$iniMes = $endMonth;
			$iniAno = $endYear;	
		}
		$chart->setArray("data",$data);
		$_SESSION['graficoBusca'] = $chart;		
		
		$size = count($_SESSION['graficoBusca']->get("data"));
		$chart = $_SESSION['graficoBusca']->get("data");
		
	} catch (ParseException $ex) {
		// The login failed. Check error to see why.
		echo "Error: " . $ex->getCode() . " " . $ex->getMessage();
	}
	
?>
	<!-- css local -->
	<style type="text/css" media="all">		
    </style>

	<div class="container">
		<h2>Especialidades</h2>
		<div class="form-group" style="margin-bottom: 0px">
			<div class="col-xs-12">
				<form class="form-horizontal" role="form" action="" method="GET" enctype="multipart/form-data">
					<fieldset>
						<div class="form-group">
							<div class="col-xs-2">
								<label for="dtInicio">Inicio</label>
								<input id="dtInicio" name="dtInicio" class="form-control datepicker" type="date"
								value="<?php echo $_SESSION['graficoBusca']->get("dtInicio");?>">
							</div>
							<div class="col-xs-2">
								<label for="dtFim">Fim</label>
								<input id="dtFim" name="dtFim" class="form-control datepicker" type="date" placeholder=""
								value="<?php echo $_SESSION['graficoBusca']->get("dtFim");?>">
							</div>							
							<div class="col-xs-3">
								<label for="tipoBusca">Tipo Atendimento</label><br/>
								<input type="radio" name="tipoBusca" value="0" 
								<?php if($_SESSION['graficoBusca']->get('tipoBusca') == 0){echo 'checked';}?>>  Usuários 
								<input type="radio" name="tipoBusca" value="1" 
								<?php if($_SESSION['graficoBusca']->get('tipoBusca') == 1){echo 'checked';}?>>  Monitores 
								<input type="radio" name="tipoBusca" value="2" 
								<?php if($_SESSION['graficoBusca']->get('tipoBusca') == 2){echo 'checked';}?>>  Todos
							</div>
						</div>	
						<button id="buscarGraph" type="submit" class="btn btn-success">Buscar</button>
					</fieldset>
				</form>
			</div>
		</div>
	
	    <div class="form-group">
			<div class="col-xs-12">
				<form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
					<fieldset>
						<div class="form-group">
							<div class="col-xs-12">
								<canvas id="graficoNovosUsuarios" width="800" height="350"></canvas>							
							</div>
						</div>						
					</fieldset>
				</form>			
			</div>			
		</div>
		
	</div>
	
	<script type="text/javascript">
		function getMes(num, txt){
			var mes = '';
			switch(num){
				case 1:
					mes = 'Jan';
					break;
				case 2:
					mes = 'Fev';
					break;
				case 3:
					mes = 'Mar';
					break;
				case 4:
					mes = 'Abr';
					break;
				case 5:
					mes = 'Mai';
					break;
				case 6:
					mes = 'Jun';
					break;
				case 7:
					mes = 'Jul';
					break;
				case 8:
					mes = 'Ago';
					break;
				case 9:
					mes = 'Set';
					break;
				case 10:
					mes = 'Out';
					break;
				case 11:
					mes = 'Nov';
					break;
				case 12:
				default:
					mes = 'Dez';
					break;
			}
			return mes + '/' + txt;
		};

	</script>
	
<!--		/* Script para graficos de busca*/	-->
	<script>	
		var ctx = document.getElementById("graficoNovosUsuarios").getContext('2d');
		var myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: [
				<?php 
					$size = count($_SESSION['graficoBusca']->get("data"));
					$chart = $_SESSION['graficoBusca']->get("data");

					for($x=0;$x<$size;$x++){
						if($x == $size-1){
							echo 'getMes(' . $chart[$x][0] . ',"' . $chart[$x][3] . '")';
						} else {
							echo 'getMes(' . $chart[$x][0] . ',"' . $chart[$x][3] . '"),';
						}
					}
				?>
				],
				datasets: [{
					label: 'Qtd Atend Espec',  
					data: [
					<?php 
						$size = count($_SESSION['graficoBusca']->get("data"));
						$chart = $_SESSION['graficoBusca']->get("data");

						for($x=0;$x<$size;$x++){
							if($x == $size-1){
								echo $chart[$x][2] . '';
							} else {
								echo $chart[$x][2] . ',';
							}
						}
					?>
					],
					backgroundColor: [
						<?php 
							$size = count($_SESSION['graficoBusca']->get("data"));
							$chart = $_SESSION['graficoBusca']->get("data");

							for($x=0;$x<$size;$x++){
								if($x == $size-1){
									echo "'rgba(255, 99, 132, 0.2)'";
								} else {
									echo "'rgba(255, 99, 132, 0.2)',";
								}
							}
						?>
						],
					borderColor: [
						<?php 
							$size = count($_SESSION['graficoBusca']->get("data"));
							$chart = $_SESSION['graficoBusca']->get("data");

							for($x=0;$x<$size;$x++){
								if($x == $size-1){
									echo "'rgba(255, 99, 132, 1)'";
								} else {
									echo "'rgba(255, 99, 132, 1)',";
								}
							}
						?>
						],
					borderWidth: 2,
					fill: true
				},
				{
					label: 'Qtd Atend Mês',  
					data: [
					<?php 
						$size = count($_SESSION['graficoBusca']->get("data"));
						$chart = $_SESSION['graficoBusca']->get("data");

						for($x=0;$x<$size;$x++){
							if($x == $size-1){
								echo $chart[$x][4] . '';
							} else {
								echo $chart[$x][4] . ',';
							}
						}
					?>
					],
					backgroundColor: [
						<?php 
							$size = count($_SESSION['graficoBusca']->get("data"));
							$chart = $_SESSION['graficoBusca']->get("data");

							for($x=0;$x<$size;$x++){
								if($x == $size-1){
									echo "'rgba(35,199,35,0.2)'";
								} else {
									echo "'rgba(35,199,35,0.2)',";
								}
							}
						?>
						],
					borderColor: [
						<?php 
							$size = count($_SESSION['graficoBusca']->get("data"));
							$chart = $_SESSION['graficoBusca']->get("data");

							for($x=0;$x<$size;$x++){
								if($x == $size-1){
									echo "'rgba(65,229,65,1)'";
								} else {
									echo "'rgba(65,229,65,1)',";
								}
							}
						?>
						],
					borderWidth: 2,
					fill: true
				}]
			},
			options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Especialidade mais popular x Mês'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mês'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Qtd Atendimentos'
                        },
						ticks: {
							beginAtZero:true
						}
                    }]
                }
            }
		});
	</script>
	

	
<?php
	include 'footer.php';
?>