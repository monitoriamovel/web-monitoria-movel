<?php 
	$nome_pag = "Info";
	include 'header.php';
		
	if(!isset($_SESSION['user'])){
		header("location: index");
	} else {
		$user = $_SESSION['user'];
	}
	
	use Parse\ParseObject;
	use Parse\ParseQuery;
	use Parse\ParseACL;
	use Parse\ParsePush;
	use Parse\ParseUser;
	use Parse\ParseInstallation;
	use Parse\ParseException;
	use Parse\ParseAnalytics;
	use Parse\ParseFile;
	use Parse\ParseCloud;
	use Parse\ParseClient;
	
?>

	<!-- css local -->
	<style type="text/css" media="all">
    </style>

	<div class="container">
	    <div class="form-group">
			<div class="col-xs-3" >
			<h3>Informações</h3>
				<form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
					<fieldset>
						<div class="form-group">
							<div class="col-xs-12">
								<label for="nomeInst">Nome da Instituição: <?php echo $user->get("nome"); ?> </label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<label for="nomeMonitor">Endereço: <?php echo $user->get("endereco"); ?> </label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<label for="nomeMonitor">Quantidade de monitores: 
									<?php 
										$query = new ParseQuery("usuario");
										$query->equalTo("verificado", 1);
										$query->equalTo("ativo",1);
										echo $query->count();									
									?> </label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<label for="nomeMonitor">Quantidade de usuarios:
									<?php 
										$query = new ParseQuery("usuario");
										$query->equalTo("verificado", 0);
										echo $query->count();									
									?> </label>
							</div>
						</div>
						
						<button type="submit" class="btn btn-success" style="visibility: hidden">Alterar senha</button>
						
					</fieldset>
				</form>

				<script type="text/javascript">
				</script>
			
			</div>
			<div class="col-xs-7" >
				<h3>Log de Avaliações</h3>
				<table id="tabelaMonitores" class="table table-hover">
					<thead>
						<tr>
							<th>Data</th>
							<th>Monitor</th>
							<th>Avaliacao</th>
						</tr>
					</thead>
					<tbody>
						<?php
							try {
								$query = new ParseQuery("atendimento");	
								$query->limit(500);		
								
								$mes = (date("m")-1);
								$dtInicio = new DateTime(date("Y") . '-' . ($mes < 10 ? '0'.$mes : $mes) . '-01'); 
								$dtInicio->setTime(0,0,0);
								
								$query->greaterThanOrEqualTo('createdAt',$dtInicio);
								$query->descending("createdAt");
								$query->select(["createdAt","monitor","avaliacao","ds_aval"]);								
								
								$query->greaterThanOrEqualTo('avaliacao',0);
								
								$resultPer = $query->find();
								
								for ($i = 0; $i < count($resultPer); $i++) {
								  $avaliacao = $resultPer[$i];
								  $monitor_aval = $avaliacao->get("monitor");
								  $monitor_aval->fetch();
								  if($monitor_aval->get('verificado') == 1){
									  $nome = $monitor_aval->get('nome') . ($monitor_aval->get('ativo') == 0 ? '(Removido)' : '');
									  echo '	<tr>
													<td>' . $avaliacao->getCreatedAt()->format('Y/m/d H:i') . '</td>
													<td>' . $nome . '</td>
													<td>' . $avaliacao->get('avaliacao') . '</td>
												</tr>';
								  }
								}								
							} catch (ParseException $ex) {
								// The login failed. Check error to see why.
								echo "Error: " . $ex->getCode() . " " . $ex->getMessage();
							}
        				?>
					</tbody>
				</table>
				
				</div>
			
		</div>
	</div>
	
	<script type="text/javascript">
	</script>
	
<?php
	include 'footer.php';
?>