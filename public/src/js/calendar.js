var date = new Date()
var dia = date.getDate()
var mes = date.getMonth()
var ano = date.getFullYear()
var qtdDiasMes = 0;

var meses = ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"]

function defineTituloMes() {
  dateStr = mesEscrito(mes) + " - " + ano;
  $("#calendarMesTitulo").html(dateStr);

  qtdDiasMes=qtdDiasNoMes();
  criaCelulaDiasMes();
}

function criaCelulaDiasMes() {

  var tableStr = "";

  var contadorDia = 1;

  var anoStr = "" + ano;
  var mesStr = "" + mes < 10 ? "0"+(mes+1) : mes;
  var diaComecaMes = new Date(anoStr + "-" + mesStr + "-01").getDay()+1;
  diaComecaMes = (diaComecaMes===7) ? 0 : diaComecaMes;

  var ignoraDias = 0;
  var qtdeColunas = diaComecaMes>=6 && qtdDiasMes > 30 ? 6 : 5;

  for (var i = 0; i < qtdeColunas; i++) {
    tableStr += "<tr>";
    for (var j = 0; j < 7; j++) {
      if(contadorDia <= qtdDiasMes) {
        if(diaComecaMes > ignoraDias) {
          tableStr += "<td><div></div></td>";
          ignoraDias++;
        } else {
          var diaSql = ano+'-'+mesStr+'-'+ (contadorDia < 10 ? "0"+(contadorDia) : contadorDia);
          tableStr += "<td><div id='calendarDia' title="+diaSql+">";
          tableStr += "<p id='diaNumero'>" + (contadorDia) + "</p>";

          // COLOCAR ALGUNS DADOS QUE TALVEZ SERÃO APRESENTADOS NO CALENDÁRIO
          $.get('./getAlugueisMes.php?data='+diaSql, function(data) {
            var obj = JSON.parse(data);
            if(obj.qtd == '1'){
              $('div[title="'+obj.data+'"]').append("<div class='btn btn-success btn-primary btn-block' onclick='viewEventos(\""+obj.data+"\")' data-toggle='modal' data-target='#modalAluguel'>"+obj.qtd+" aluguel</div>");
            }else if(obj.qtd > '1'){
              $('div[title="'+obj.data+'"]').append("<div class='btn btn-success btn-primary btn-block' onclick='viewEventos(\""+obj.data+"\")' data-toggle='modal' data-target='#modalAluguel'>"+obj.qtd+" alugueis</div>");
            }
          });

          tableStr += "</div></td>";
          contadorDia++;
        }
      } else {
        tableStr += "<td><div id='calendarDia'></div></td>";
      }
    }
    tableStr += "</tr>";
  }

  $("#tabelaDias").html(tableStr);

}

function viewEventos(dia) {
  $('#tabelaItemAluguel').html('');
  $.get('./getAlugueisDia.php?data='+dia, function(data) {
    console.log(dia);
    console.log(data);
    var obj = JSON.parse(data);
    for (var i = 1; i < obj.length; i++) {
      $('#tabelaItemAluguel').append('<tr>'
                 +'<td>'+obj[i].nmcliente+'</td>'
                 +'<td>'+obj[i].vlpago+'</td>'
                 +'<td>'
                   +'<button class="btn btn-warning btn-sm" onclick="editarAluguel('+ obj[i].idaluguel + ')"><i class="fa fa-pencil" aria-hidden="true"></i></button>'
                 +'</td>'
               +'</tr>');


    }
  });
}

function mesEscrito(i) {
  return meses[i];
}

function qtdDiasNoMes() {
  return new Date(ano, mes+1, 0).getDate();
}

function changeMonth(i) {
  mes+=i;
  if(mes < 0) {
    mes=11;
    ano--;
  } else if (mes > 11) {
    mes=0;
    ano++;
  }
  defineTituloMes();
}