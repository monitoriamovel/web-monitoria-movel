<?php
	session_start();

	require $_SERVER['DOCUMENT_ROOT'] . '/parseConfig.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/utilities.php';
		
	if(!isset($_SESSION['user'])){
		header("location: index");
	} else {
		$user = $_SESSION['user'];
	}
		
	use Parse\ParseObject;
	use Parse\ParseQuery;
	use Parse\ParseACL;
	use Parse\ParsePush;
	use Parse\ParseUser;
	use Parse\ParseInstallation;
	use Parse\ParseException;
	use Parse\ParseAnalytics;
	use Parse\ParseFile;
	use Parse\ParseCloud;
	use Parse\ParseClient;

	if ($_SERVER ["REQUEST_METHOD"] == "POST") {
		try {
			$prob = array(false,'',null);
			$string = '';
			if(isset($_SESSION['monitor']) && $_SESSION['monitor'] == 'new'){
				$monitor = new ParseObject("usuario");
				$monitor->set("usuario",$_POST ['usuarioMonitor']);
				
				$senha = '';
				for($x = 0; $x < 6; $x++){
					$senha = $senha . '' . rand(0,9);
				}				
				
				$monitor->set("senha",md5($senha));
				$monitor->set("verificado",1);
				
				$query = new ParseQuery("usuario");
				$query->equalTo("email",$_POST['emailMonitor']);
				$results = $query->find();
				
				if(count($results) > 0){
					$prob[0] = true;
					$prob[1] = 'Já existe um usuario com este email!';
					$prob[2] = null;
				}
				
				$query = new ParseQuery("usuario");
				$query->equalTo("usuario",$_POST['usuarioMonitor']);
				$results = $query->find();
				
				if(count($results) > 0){
					$prob[0] = true;
					$prob[1] = 'Ja existe um usuario com este login!';
					$prob[2] = null;
				}
				
				if(!$prob[0]){
					$string = 'Monitor criado com sucesso!' .
					'\nUsuario: ' . $_POST ['usuarioMonitor'] .
					'\nSenha: ' . $senha;
				}
			} else {
				$query = new ParseQuery("usuario");
				$monitor = $query->get($_SESSION['monitor']);
				
				$query = new ParseQuery("usuario");
				$query->equalTo("email",$_POST['emailMonitor']);
				$usuario = $query->first();
				
				if($monitor->getObjectId() != $usuario->getObjectId()){
					$prob[0] = true;
					$prob[1] = 'Já existe um usuario com este email!';
					$prob[2] = $monitor->getObjectId();
				}
				
				$query = new ParseQuery("usuario");
				$query->equalTo("usuario",$_POST['usuarioMonitor']);
				$usuario = $query->first();
				
				if($monitor->getObjectId() != $usuario->getObjectId()){
					$prob[0] = true;
					$prob[1] = 'Já existe um usuario com este login!';
					$prob[2] = $monitor->getObjectId();
				}
				
				if(!$prob[0]){
					$string = 'Monitor salvo com sucesso!' .
					'\nUsuario: ' . $_POST ['usuarioMonitor'];
				}
			}
			
			if(!$prob[0]){							
				$monitor->set("nome",$_POST ['nomeMonitor']);
				
				$dt_nasc = explode('-', $_POST ['nascimentoMonitor']);
				$monitor->set("data_nascimento", ($dt_nasc[2] . '/' . $dt_nasc[1] . '/' . $dt_nasc[0]));
				
				$monitor->set("email",$_POST['emailMonitor']);
				$monitor->set("especialidades",$_POST ['especialidadesMonitor']);
				$monitor->set("ativo",1);

				$file = ParseFile::createFromFile($_POST['monitorImg'], "file.bin");
				$file->save();
					
				$monitor->set("foto", $file);
							
				$monitor->save();
				
				$_SESSION['monitor'] = null;
				$_SESSION['msg'] = $string;
			} else {
				$_SESSION['err'] = $prob[2];
				$_SESSION['monitor'] = null;
				$_SESSION['msg'] = $prob[1];
			}
				header('location: monitores');
		} catch (ParseException $ex) {
			// The login failed. Check error to see why.
			echo "Error: " . $ex->getCode() . " " . $ex->getMessage();
		}
	}	
?>