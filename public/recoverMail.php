<?php
    $nome_pag = "Login";

	//https://github.com/PHPMailer/PHPMailer	
	require $_SERVER['DOCUMENT_ROOT'] . '/parseConfig.php';
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Monitoria Movel - <?php echo $nome_pag ?></title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> 

    <link href="src/css/style.css" rel="stylesheet">
	
  </head>
  <body>

<?php
	if(isset($_GET['email'])){
		$mail = new PHPMailer;

		//$mail->SMTPDebug = 3;                               // Enable verbose debug output

		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'mail.smtp2go.com;mail.smtp2go.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'monitoria@movel.com';                 // SMTP username
		$mail->Password = 'ujmnhy67';                           // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to

		$mail->setFrom('monitoriamovel@exemple.com', 'Monitoria Movel FURB');
		$mail->addAddress($_GET['email']);     // Add a recipient
		$mail->addReplyTo('monitoriamovel@exemple.com', 'Monitoria Movel FURB');
			
		$mail->isHTML(true);                                  // Set email format to HTML

		$senha = '';
		
		for($x = 0; $x < 6; $x++){
			$senha = $senha . '' . rand(0,9);
		}
		
		$mail->Subject = 'Nova senha';
		$mail->Body    = 'A seu pedido, uma nova senha foi gerada para a sua conta. Certifique-se de troca-la assim que possivel: <b>' . $senha . '</b>';
		$mail->AltBody = 'A seu pedido, uma nova senha foi gerada para a sua conta. Certifique-se de troca-la assim que possivel: ' . $senha;

		if(!$mail->send()) {
			$query = new ParseQuery("usuario");
			$query->equalTo("email",$_GET['email']);
			$usuario = $query->first();
			$usuario->set("senha",$senha);
			$usuario->save();
			echo 'O Email não pôde ser enviado! Por favor tente novamente em alguns minutos.';
//			echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			echo 'Email enviado com sucesso!';
		}
	} else {
		header("location: index");
	}
?>

  </body>
</html>