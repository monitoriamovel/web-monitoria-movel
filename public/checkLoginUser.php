<?php
	session_start();

	require $_SERVER['DOCUMENT_ROOT'] . '/parseConfig.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/utilities.php';
		
	use Parse\ParseObject;
	use Parse\ParseQuery;
	use Parse\ParseACL;
	use Parse\ParsePush;
	use Parse\ParseUser;
	use Parse\ParseInstallation;
	use Parse\ParseException;
	use Parse\ParseAnalytics;
	use Parse\ParseFile;
	use Parse\ParseCloud;
	use Parse\ParseClient;

	if ($_SERVER ["REQUEST_METHOD"] == "POST") {
		try {
			$username = escape_all_specials ( $_POST ['username'] );
			$password = escape_all_specials ( md5($_POST ['password']) );
			$bool = false;
			
			$query = new ParseQuery("instituicao");
			$query->equalTo("usuario",$username);
			$query->equalTo("senha",$password);

			$user = $query->first();
			if($user != null){
				//usuario encontrado
				//salva usuario da sessao e redireciona para pagina inicial
				
				$_SESSION['user'] = $user;
				header('location: monitores');
			} else {
				//nenhum usuario com esse username ou senha

				$_SESSION['err'] = true;
				//redireciona para index
				header('location:index');
			}
		} catch (ParseException $ex) {
			// The login failed. Check error to see why.
			echo "Error: " . $ex->getCode() . " " . $ex->getMessage();
		}
	}	
?>