<?php 
	$nome_pag = "Monitores";
	include 'header.php';

	use Parse\ParseObject;
	use Parse\ParseQuery;
	use Parse\ParseACL;
	use Parse\ParsePush;
	use Parse\ParseUser;
	use Parse\ParseInstallation;
	use Parse\ParseException;
	use Parse\ParseAnalytics;
	use Parse\ParseFile;
	use Parse\ParseCloud;
	use Parse\ParseClient;
	
	if(!isset($_SESSION['user'])){
		header("location: index");
	} else {
		$user = $_SESSION['user'];
	}
	
	if(isset($_SESSION['msg'])){
		echo "<script language='javascript'>alert('" . $_SESSION['msg'] ."');</script>"; // Prompts the user
		$_SESSION['msg'] = null;
	}

	try {		
		$monitorFiltro = new ParseObject("usuario");
		$monitorFiltro->set("nomeMonitor","");
		$monitorFiltro->set("emailMonitor","");
		$monitorFiltro->set("espMonitor","");
		$filtro = "nomeFiltro=";
		if(isset($_GET["nomeFiltro"]) && trim($_GET["nomeFiltro"])) {
			$monitorFiltro->set("nomeMonitor",$_GET['nomeFiltro']);
			 $filtro = $filtro . $_GET['nomeFiltro'];
		}
		$filtro = $filtro . "&emailFiltro=";
		if(isset($_GET["emailFiltro"]) && trim($_GET["emailFiltro"])) {
			$monitorFiltro->set("emailMonitor",$_GET['emailFiltro']);
			$filtro = $filtro . $_GET['emailFiltro'];
		}
		$filtro = $filtro . "&espFiltro=";
		if(isset($_GET["espFiltro"]) && trim($_GET["espFiltro"])) {
			$monitorFiltro->set("espMonitor",$_GET['espFiltro']);
			$filtro = $filtro . $_GET['espFiltro'];
		}
		$monitorFiltro->set("linkFiltro", $filtro . "&");
		$_SESSION['monitorFiltro'] = $monitorFiltro;	
	} catch (ParseException $ex) {
		// The login failed. Check error to see why.
		echo "Error: " . $ex->getCode() . " " . $ex->getMessage();
	}
	
?>

	<!-- css local -->
	<style type="text/css" media="all">
		.btn-success {
			background-color: #005FA4;
			border-color: #005FA4;
		}
		.btn-success:hover{
			background-color: #00549A;
		}
		.btn-success:active{
			background-color: #00549A;
		}
		
		.trClicable{
		
		}
    </style>

	<!-- JQuery Script -->
	<script>
	$(document).ready(function(){
		$("tr[class*='trClicable']").click(function(){
//			alert($(this).find("td:first").html());
			editarMonitor($(this).find("td:first").html(), <?php echo '"' . $_SESSION['monitorFiltro']->get("linkFiltro") . '"';?>);
		});
		
		$("button[id*='btTabMonitores']").click(function(){
			var d = new Date();
			$("#tabelaMonitores").table2excel({    // exclude CSS class
				exclude: ".noExport",
				name: "Dados Monitores",
				filename: "tabela_monitores_" + d.getFullYear() + "_" + (d.getMonth()+1) + "_" + d.getDate() //do not include extensions
			});
		});
		
	});
	
	

	</script>
	
	<script type="text/javascript">
	function readURL(input) {
		if (input.files && input.files[0] && input.files[0].size <= 6150000) { //6000000 / 1024 kb
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#fotoShow').css({'visibility':'visible','width':'100%','height':'auto'}).attr('src',
						e.target.result);
				document.getElementById('monitorImg').value = e.target.result;
			};
			
			reader.readAsDataURL(input.files[0]);
		} else {
			alert('Imagem muito grande! Tamanho máximo de imagem: 6mb');
		}
	};

	function setTextoNosCampos(ie_new, nome = "", usuario = "", data_nascimento = "01/01/2000", especialidades = "", 
						email = "", foto = "", avTotal = "0", avMes = "0", medTotal = "0", medMes = "0") {
		document.getElementById('nomeMonitor').value = nome;
		document.getElementById('nomeMonitor').readOnly = false;
		
		document.getElementById('usuarioMonitor').value = usuario;
		document.getElementById('usuarioMonitor').readOnly = !ie_new;
		
		document.getElementById('nascimentoMonitor').value = data_nascimento.split('/')[2]+'-'+data_nascimento.split('/')[1]+'-'+data_nascimento.split('/')[0];
		document.getElementById('nascimentoMonitor').readOnly = false;
		
		document.getElementById('especialidadesMonitor').value = especialidades;		
		document.getElementById('especialidadesMonitor').readOnly = false;
		
		document.getElementById('emailMonitor').value = email;		
		document.getElementById('emailMonitor').readOnly = false;
		
		$('#imgInput').css({'visibility':'visible'});
		
		if(foto == ""){
			foto = 'src/img/foto.png';
		}
		document.getElementById('fotoShow').setAttribute('src', foto);
		document.getElementById('monitorImg').value = foto;
		
		$('#qtdAvalTotal').text("Avaliações total: " + avTotal);		
		$('#qtdAvalMes').text("Avaliações mês: " + avMes);
		$('#medAvalTotal').text("Média avaliações total: " + medTotal);
		$('#medAvalMes').text("Média avaliações mês: " + medMes);
		
		enableSaveMonitor();
		if(!ie_new){
			enableDeleteMonitor();
		} else {
			disableDeleteMonitor();
		}
	}
	
	function disableFields() {
		document.getElementById('nomeMonitor').value = '';
		document.getElementById('nomeMonitor').readOnly = true;
		
		document.getElementById('usuarioMonitor').value = '';
		document.getElementById('usuarioMonitor').readOnly = true;
		
		document.getElementById('nascimentoMonitor').value = '';
		document.getElementById('nascimentoMonitor').readOnly = true;
		
		document.getElementById('especialidadesMonitor').value = '';		
		document.getElementById('especialidadesMonitor').readOnly = true;
		
		document.getElementById('emailMonitor').value = '';		
		document.getElementById('emailMonitor').readOnly = true;
		
		$('#imgInput').css({'visibility':'hidden'});
				
		$('#qtdAvalTotal').text("Avaliações total: 0");		
		$('#qtdAvalMes').text("Avaliações mês: 0");
		$('#medAvalTotal').text("Média avaliações total: 0");
		$('#medAvalMes').text("Média avaliações mês: 0");
	}
	
	function enableSaveMonitor() {
		document.getElementById("saveMonitor").disabled = false;
	}
	function disableSaveMonitor() {
		document.getElementById("saveMonitor").disabled = true;
	}
	
	function enableDeleteMonitor() {
		document.getElementById("deleteMonitor").disabled = false;
	}
	function disableDeleteMonitor() {
		document.getElementById("deleteMonitor").disabled = true;
	}
	
	function editarMonitor(cd_monitor, filtros = '') {
		location.href = 'monitores?' + filtros + 'id=' + cd_monitor;
	}

	function removerCelula(cel) {
	 var p=cel.parentNode.parentNode;
	     p.parentNode.removeChild(p);
	     $('#tabelaMonitores').trigger('repaginate');
	}

	  $(document).ready(function(){
	    desenhaTodasTabelas();
	  });

	  function desenhaTodasTabelas() {
	    desenhaPaginacao('#tabelaMonitores');
	  }

	  function desenhaPaginacao(idTabela) {
	    // Adaptado de:  http://gabrieleromanato.name/jquery-easy-table-pagination/
	    $(idTabela).each(function() {
	      var currentPage = 0;
	      var numPerPage = 10; //limite de registros por pagina
	      var $table = $(this);
	      $table.bind('repaginate', function() {
	          $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
	      });
	      $table.trigger('repaginate');
	      var numRows = $table.find('tbody tr').length;
	      var numPages = Math.ceil(numRows / numPerPage);
	      var $pager = $('<div class="pager"></div>');
	      for (var page = 0; page < numPages; page++) {
	          $('<span class="page-number"></span>').text(page + 1).bind('click', {
	              newPage: page
	          }, function(event) {
	              currentPage = event.data['newPage'];
	              $table.trigger('repaginate');
	              $(this).addClass('active').siblings().removeClass('active');
	          }).appendTo($pager).addClass('clickable');
	      }
	      $pager.insertAfter($table).find('span.page-number:first').addClass('active');
	    });
	  }	  
	</script>
	
	<div class="container">	
		<form class="form-horizontal" role="form" action="" method="GET" enctype="multipart/form-data">
			<fieldset>
				<div class="form-group">
					<div class="col-xs-3">
						<label for="nomeFiltro">Nome monitor</label>
						<input id="nomeFiltro" name="nomeFiltro" class="form-control" type="text" placeholder=""
						value="<?php echo $_SESSION['monitorFiltro']->get("nomeMonitor");?>">
					</div>
					<div class="col-xs-3">
						<label for="emailFiltro">Email monitor</label>
						<input id="emailFiltro" name="emailFiltro" class="form-control" type="text" placeholder=""
						value="<?php echo $_SESSION['monitorFiltro']->get("emailMonitor");?>">
					</div>
					<div class="col-xs-3">
						<label for="espFiltro">Especialidades</label>
						<input id="espFiltro" name="espFiltro" class="form-control" type="text" placeholder=""
						value="<?php echo $_SESSION['monitorFiltro']->get("espMonitor");?>">
					</div>
				</div>	
				<button type="submit" class="btn btn-success">Pesquisar</button>
			</fieldset>
		</form>
	
		<!-- TABELA MONITORES-->
	    <div class="form-group" style="margin-left: 0%">
					<!-- FILTRO MONITORES  -->
			<div class="col-xs-7">
				<div class="form-group">
					<h3>Lista de Monitores</h3>
				</div>
				<table id="tabelaMonitores" class="table table-hover">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Usuario</th>
							<th>Data de Nascimento</th>
							<th>Especialidades</th>
						</tr>
					</thead>
					<tbody>
						<?php
							try {
								$monitorFiltro = new ParseObject("usuario");
								$monitorFiltro->set("nomeMonitor","");
								$monitorFiltro->set("emailMonitor","");
								$monitorFiltro->set("espMonitor","");
		
								$query = new ParseQuery("usuario");
								$query->equalTo("verificado",1);
								$query->equalTo("ativo",1);
								
								if(isset($_GET["nomeFiltro"]) && trim($_GET["nomeFiltro"])) {
									$query->contains("nome",escape_all_specials( $_GET['nomeFiltro']));
									$monitorFiltro->set("nomeMonitor",$_GET['nomeFiltro']);
								}
								if(isset($_GET["emailFiltro"]) && trim($_GET["emailFiltro"])) {
									$query->contains("email",escape_all_specials($_GET['emailFiltro']));
									$monitorFiltro->set("emailMonitor",$_GET['emailFiltro']);
								}
								if(isset($_GET["espFiltro"]) && trim($_GET["espFiltro"])) {
									$query->contains("especialidades",escape_all_specials($_GET['espFiltro']));
									$monitorFiltro->set("espMonitor",$_GET['espFiltro']);
								}
	
		
								$_SESSION['monitorFiltro'] = $monitorFiltro;
								
								$query->select(["id","nome","usuario","data_nascimento","especialidades"]);								
								
								$resultPer = $query->find();

								for ($i = 0; $i < count($resultPer); $i++) {
								  $monitor = $resultPer[$i];
								  echo '	<tr class="trClicable">
												<td hidden class="noExport">' . $monitor->getObjectId() .'</td>
												<td>' . $monitor->get('nome') . '</td>
												<td>' . $monitor->get('usuario') . '</td>
												<td>' . nvl($monitor->get('data_nascimento'),'Não informado') . '</td>
												<td>' . nvl($monitor->get('especialidades'),'Não informado') . '</td>
											</tr>';
								}
								
								echo '<script type="text/javascript">',
								 'disableDeleteMonitor();',
								 '</script>';
								
							} catch (ParseException $ex) {
								// The login failed. Check error to see why.
								echo "Error: " . $ex->getCode() . " " . $ex->getMessage();
							}
        				?>
					</tbody>
				</table>
				<button id="btTabMonitores" class="btn btn-success" >Exportar busca</button>

	      	</div>
			
			<div class="col-xs-5">
			<h3>Monitor Selecionado</h3>
				<form class="form-horizontal" role="form" action="saveMonitor" method="post" enctype="multipart/form-data">
					<fieldset>
						<div class="form-group">
							<div class="col-xs-6">
								<img id="fotoShow" src="src/img/foto.png"
										alt="Foto Monitor" class="form-control"
										style="width:auto;height:auto;max-height:100%;max-width:100%;" />

							</div>

							<div class="col-xs-6"> 
								<div class="form-group">
									<div class="form-group">
										<div class="col-xs-12">
											<label id="qtdAvalTotal" name="qtdAvalTotal">Avaliações total: 0</label>
										</div>
									</div>
									<div class="form-group">
										<div class="col-xs-12">
											<label id="qtdAvalMes" name="qtdAvalMes">Avaliações mês: 0</label>
										</div>
									</div>
									<div class="form-group">
										<div class="col-xs-12">
											<label id="medAvalTotal" name="medAvalTotal">Média avaliações total: 0</label>
										</div>
									</div>
									<div class="form-group">
										<div class="col-xs-12">
											<label id="medAvalMes" name="medAvalMes">Média avaliações mês: 0</label>
										</div>
									</div>			
								</div>
							</div>						
						</div>
	
						<input id="imgInput" class="form-control" type="file" onchange="readURL(this);"
								accept="image/x-png, image/jpeg" name="image" style="visibility: hidden;margin-top:-20px"/>	
								
						<input id="monitorImg" name="monitorImg" class="form-control" type="text" readonly="true" style="visibility:hidden">
						
						<div class="form-group" style="margin-top: 10px">
							<div class="col-xs-12">
										<label for="nomeMonitor">Nome</label>
										<input id="nomeMonitor" name="nomeMonitor" class="form-control" type="text" readOnly="true" required="true">
									</div>							
						</div>
						
						<div class="form-group">
							<div class="col-xs-6">
								<label for="usuarioMonitor">Usuario</label>
								<input id="usuarioMonitor" required="true" name="usuarioMonitor" class="form-control" type="text" readonly="true">
							</div>
							<div class="col-xs-6">							
								<label for="nascimentoMonitor">Data de Nascimento</label>
								<input id="nascimentoMonitor" required="true" name="nascimentoMonitor" class="form-control datepicker" type="date" readonly="true">
							</div>							

						</div>
						
						<div class="form-group">
							<div class="col-xs-12">
								<label for="emailMonitor">Email</label>
								<input id="emailMonitor" required="true" name="emailMonitor" class="form-control" type="text" readonly="true">
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-xs-12">
								<label for="especialidadesMonitor">Especialidades</label>
								<input id="especialidadesMonitor" required="" name="especialidadesMonitor" class="form-control" type="textarea" placeholder="" readonly="true">
							</div>
						</div>
						
						<button id="saveMonitor" type="submit" class="btn btn-success" >Salvar</button>
						
						<button id="deleteMonitor" type="submit" class="btn btn-success" style="" formaction="deleteMonitor"><i class="fa fa-minus" aria-hidden="true"></i> Deletar Monitor</button>
						
						<button type="submit" class="btn btn-success" style="float: right" formaction="newMonitor"><i class="fa fa-plus" aria-hidden="true"></i> Novo Monitor</button>
						
					</fieldset>
				</form>
			
			</div>
		</div>
	</div>

	<?php
		if((isset($_SESSION['err'])) or (isset($_GET["id"]) && trim($_GET["id"]))) {
//			
			$id = 0;
			if(isset($_SESSION['err'])){
				$id = $_SESSION['err'];
				$_SESSION['err'] = null;
			} else {
				$id = $_GET["id"];
			}
			try {
				//CALCULA TOTAL AVALIACOES
			
				$query = new ParseQuery("usuario");
				$monitor = $query->get($id);
				
				$query = new ParseQuery("usuario_avaliacao");
				$query->equalTo("monitor",$monitor);
				
				$results = $query->find();
				
				$aval_total = 0;
				$aval_mes = 0;
				
				for ($i = 0; $i < count($results); $i++) {
				  $object = $results[$i];
				  $aval_total = $aval_total + $object->get('nota');
				}
				
				$med_aval_total = (count($results) > 0 ? number_format($aval_total/count($results), 2) : 0);
				$aval_total = count($results);

				//CALCULA MES ATUAL AVALIACOES
				
				$query = new ParseQuery("usuario");
				$monitor = $query->get($id);
				
				$query = new ParseQuery("usuario_avaliacao");
				$query->equalTo("monitor",$monitor);
								
				$mesProx = new DateTime(((date('m') < 12) ?  date('Y') . '-' . ((date('m')+1 < 10) ? '0' . (date('m')+1) : date('m')+1) : ((date('Y')+1))). '-01');
				$mesProx->setTime(0,0,0);

				$mesAtual = new DateTime(date('Y-m') . '-01');
				$mesAtual->setTime(0,0,0);	
				
				$query->greaterThanOrEqualTo('createdAt',$mesAtual);
				$query->lessThan('createdAt',$mesProx);
				
				$results = $query->find();
				
				for ($i = 0; $i < count($results); $i++) {
				  $object = $results[$i];
				  $aval_mes = $aval_mes + $object->get('nota');
				}
				
				$med_aval_mes = (count($results) > 0 ? number_format($aval_mes/count($results), 2) : 0);			
				$aval_mes = count($results);				
				
				$_SESSION['monitor'] = $id;			
				
								
				echo '<script type="text/javascript">',
				 'setTextoNosCampos(false,"' 
				 . $monitor->get('nome') . '","' 
				 . $monitor->get('usuario') . '","'
				 . $monitor->get('data_nascimento') . '","'
				 . $monitor->get('especialidades') . '","'
				 . $monitor->get('email') . '","'
				 . $monitor->get('foto')->getURL() . '","'
				 . $aval_total . '","'
				 . $aval_mes . '","'
				 . $med_aval_total . '","'
				 . $med_aval_mes
				 . '");',
				 '</script>';
			} catch (ParseException $ex) {
				// The login failed. Check error to see why.
				echo "Error: " . $ex->getCode() . " " . $ex->getMessage();
			}
		} else if(isset($_SESSION['monitor']) and $_SESSION['monitor'] == 'new'){
			//Adicionando novo Monitor
			echo '<script type="text/javascript">',
				 'setTextoNosCampos(true);',
				 '</script>';			
		} else {
			echo '<script type="text/javascript">',
				 'disableSaveMonitor();',
				 'disableDeleteMonitor();',
				 'disableFields();',
				 '</script>';
		}
	?>	
	
<?php
	include 'footer.php';
?>