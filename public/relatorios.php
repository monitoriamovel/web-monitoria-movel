<?php 
	$nome_pag = "Relatorios";
	include 'header.php';

	if(!isset($_SESSION['user'])){
		header("location: index");
	} else {
		$user = $_SESSION['user'];
	}
	
	use Parse\ParseObject;
	use Parse\ParseQuery;
	use Parse\ParseACL;
	use Parse\ParsePush;
	use Parse\ParseUser;
	use Parse\ParseInstallation;
	use Parse\ParseException;
	use Parse\ParseAnalytics;
	use Parse\ParseFile;
	use Parse\ParseCloud;
	use Parse\ParseClient;
		
?>
	<!-- css local -->
	<style type="text/css" media="all">
		
    </style>
	
	<div class="container" style="margin-top: 50px">
		<div class="bs-glyphicons">
			<ul class="bs-glyphicons-list">
				<a href="relatorioEspecialidade">
					<li>
						<i class="fa fa-book" aria-hidden="true"></i>
						<span class="glyphicon-class">Relatorio<br/>Especialidades</span>
					</li>
				</a>
				<a href="relatorioUsuario">
					<li>
						<i class="fa fa-user" aria-hidden="true"></i>
						<span class="glyphicon-class">Relatorio<br/>Usuários</span>
					</li>
				</a>
				<a href="relatorioAtendimento">
					<li>
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
						<span class="glyphicon-class">Relatorios<br/>Atendimentos</span>
					</li>
				</a>
			</ul>
		</div>
	</div>
	
<?php
	include 'footer.php';
?>