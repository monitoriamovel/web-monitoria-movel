<?php
  require_once('clienteController.php');

  $nome_pag = "Cadastro de Cliente";
	require_once('header.php');


   $permissaoVer = false;
   $_SESSION['permissaoCadastrar'] = false;

   foreach ($_SESSION['Permissoes'] as $key => $value) {
     if(!is_array($value)) {
         if(stristr($nome_pag, $value)) {
           $arrayPerm = $_SESSION['Permissoes'][$value+1];

           if($arrayPerm['visualizar'] == "S") {
             $permissaoVer = true;
           }

           if($arrayPerm['cadastrar'] == "S") {
             $_SESSION['permissaoCadastrar'] = true;
           }
           break;
         }
     }
   }

   if(!$permissaoVer) {
     $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
     header("Location: cliente"); exit;
   }

?>

<div class="container">
	<form class="form-horizontal" role="form" action="" method="post">
        <fieldset>
        <h3>Informações</h3>
		    <div class="form-group">
			    <div class="col-xs-5">
			        <label for="nome">Nome Completo</label>
			        <input id="nome" name="nome" required="" class="form-control" type="text" placeholder="Nome Completo">
			    </div>
			    <div class="col-xs-3">
			        <label for="dtNascimento">Data de Nascimento</label>
			        <input id="dtNascimento" required="" name="dtNascimento" class="form-control datepicker" type="date" placeholder="Data Nascimento">
			    </div>
			    <div class="col-xs-2">
			        <label for="cpf">CPF</label>
			        <input id="cpf" name="cpf" required="" class="form-control" type="text" placeholder="CPF">
			    </div>
			    <div class="col-xs-2">
			        <label for="rg">RG</label>
			        <input id="rg" name="rg" required="" class="form-control" type="text" placeholder="RG">
			    </div>
		    </div>

			<h3>Contato</h3>
		    <div class="form-group">
			    <div class="col-xs-4">
			        <label for="telefoneCasa">Casa</label>
			        <input id="telefoneCasa" name="telefoneCasa" class="form-control" type="text" placeholder="Telefone Casa">
			    </div>
			    <div class="col-xs-4">
			        <label for="telefoneComercial">Comercial</label>
			        <input id="telefoneComercial" name="telefoneComercial" class="form-control" type="text" placeholder="Telefone Comercial">
			    </div>
          <div class="col-xs-4">
              <label for="telefoneCelular">Celular</label>
              <input id="telefoneCelular" name="telefoneCelular" class="form-control" type="text" placeholder="Telefone Celular">
          </div>
		    </div>

			<h3>Endereço</h3>
		    <div class="form-group">
              <input id="cd_endereco" name="cd_endereco" type="text" style="visibility: hidden;">

                <div class="col-xs-3">
                    <label for="enderecoEstado">Estado</label>
                    <input id="enderecoEstado" required="" name="enderecoEstado" class="form-control" type="text" placeholder="Estado">
                </div>
                <div class="col-xs-3">
                    <label for="enderecoCidade">Cidade</label>
                    <input id="enderecoCidade" required="" name="enderecoCidade" class="form-control" type="text" placeholder="Cidade">
                </div>
                <div class="col-xs-3">
                    <label for="enderecoBairro">Bairro</label>
                    <input id="enderecoBairro" required="" name="enderecoBairro" class="form-control" type="text" placeholder="Rua">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-2">
                    <label for="enderecoCep">CEP</label>
                    <input id="enderecoCep" required="" name="enderecoCep" class="form-control" type="text" placeholder="CEP">
                </div>
                <div class="col-xs-4">
                    <label for="enderecoRua">Rua</label>
                    <input id="enderecoRua" required="" name="enderecoRua" class="form-control" type="text" placeholder="Rua">
                </div>
                <div class="col-xs-1">
                    <label for="numeroEndereco">Número</label>
                    <input id="numeroEndereco" required="" name="numeroEndereco" class="form-control" type="text" placeholder="Nº">
                </div>
                <div class="col-xs-2">
                    <label for="enderecoComplemento">Complemento</label>
                    <input id="enderecoComplemento" name="enderecoComplemento" class="form-control" type="text" placeholder="Comp.">
                </div>
            </div>

		    <div class="form-group">
			    <div class="col-xs-10">
            <h3>Observação</h3>
			        <input id="observacao" name="observacao" class="form-control" type="textarea" placeholder="Observação">
			    </div>
		    </div>

			<button type="submit" class="btn btn-success">Salvar</button>
		</fieldset>
	</form>
</div>
<script type="text/javascript">


jQuery(function($){
  //  $("#dtNascimento").mask("99/99/9999");
   $("#enderecoCep").mask("99999-999");
   $("#cpf").mask("999.999.999-99");
   $("#rg").mask("9.999.999");
   $("#telefoneCasa").mask("(99) 9999-9999");
   $("#telefoneCelular").mask("(99) 9999-9999");
   $("#telefoneComercial").mask("(99) 9999-9999");
});

function setTextoNosCampos(nome, cpf, rg, dtNascimento, telCasa, telCelular, telComercial, observacao, estado, cidade, bairro, cep, rua, numero, complemento, cd_endereco) {
  document.getElementById('nome').value = nome;
  document.getElementById('cpf').value = cpf;
  document.getElementById('rg').value = rg;
  document.getElementById('dtNascimento').value = dtNascimento;
  document.getElementById('telefoneCasa').value = telCasa;
  document.getElementById('telefoneCelular').value = telCelular;
  document.getElementById('telefoneComercial').value = telComercial;
  document.getElementById('observacao').value = observacao;
  document.getElementById('enderecoEstado').value = estado;
  document.getElementById('enderecoCidade').value = cidade;
  document.getElementById('enderecoBairro').value = bairro;
  document.getElementById('enderecoCep').value = cep;
  document.getElementById('enderecoRua').value = rua;
  document.getElementById('numeroEndereco').value = numero;
  document.getElementById('enderecoComplemento').value = complemento;
  document.getElementById('cd_endereco').value = cd_endereco;
}

</script>


<?php
  $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');

  if(isset($_GET["id"]) && trim($_GET["id"]) ) {
    $resultFunc = mysqli_query($con,'SELECT * from `cliente` where `cd_cliente` = '.$_GET["id"]);
    $row = mysqli_fetch_array($resultFunc);

  if(count($row) > 0) {
    $resultEndereco = mysqli_query($con,'SELECT * from `endereco` where `cd_endereco` = '. $row['cd_endereco']);
    $rowEnd = mysqli_fetch_array($resultEndereco);

    echo '<script type="text/javascript">'
       , 'setTextoNosCampos("'. $row['nm_cliente'] .'",
                            "'. $row['nr_cpf'] .'",
                            "'. $row['nr_rg'] .'",
                            "'. $row['dt_nascimento'] .'",
                            "'. $row['nr_telefone_casa'] .'",
                            "'. $row['nr_telefone_celular'] .'",
                            "'. $row['nr_telefone_comercial'] .'",
                            "'. $row['ds_observacao'] .'",
                            "'. $rowEnd['ds_estado'] .'",
                            "'. $rowEnd['ds_cidade'] .'",
                            "'. $rowEnd['ds_bairro'] .'",
                            "'. $rowEnd['nr_cep'] .'",
                            "'. $rowEnd['ds_rua'] .'",
                            "'. $rowEnd['nr_complemento'] .'",
                            "'. $rowEnd['ds_complemento'] .'",
                            "'. $rowEnd['cd_endereco'] .'");'
        , '</script>';

  }
  mysqli_close($con);

}


	require_once('footer.php');
?>
