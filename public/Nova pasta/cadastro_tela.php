<?php
    require_once('telaController.php');

    $nome_pag = "Cadastro de Tela";
	require_once('header.php');

  $permissaoVer = false;
  $_SESSION['permissaoCadastrar'] = false;

  foreach ($_SESSION['Permissoes'] as $key => $value) {
    if(!is_array($value)) {
        if(stristr($nome_pag, $value)) {
          $arrayPerm = $_SESSION['Permissoes'][$value+1];

          if($arrayPerm['visualizar'] == "S") {
            $permissaoVer = true;
          }

          if($arrayPerm['cadastrar'] == "S") {
            $_SESSION['permissaoCadastrar'] = true;
          }
          break;
        }
    }
  }

  if(!$permissaoVer) {
    $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
    header("Location: atendente"); exit;
  }
?>

<div class="container">
	<form class="form-horizontal" role="form" action="" method="post">
        <fieldset>
		    <div class="form-group">
			    <div class="col-xs-5">
			        <label for="ex1">Nome</label>
			        <input id="nmTela" class="form-control" autofocus="" required="" type="text" placeholder="Descrição" name="nome">
			    </div>
			    <div class="col-xs-5">
			        <label for="ex1">Nome do arquivo</label>
			        <input id="dsArquivo" class="form-control" autofocus="" required="" type="text" placeholder="Descrição" name="nomeArquivo">
			    </div>
		    </div>

			<button type="submit" class="btn btn-success">Cadastrar</button>
		</fieldset>
	</form>
</div>

<script type="text/javascript">
  function setTextCampos(nmTela, dsArquivo){
    document.getElementById('nmTela').value = nmTela;
    document.getElementById('dsArquivo').value = dsArquivo;

  }
</script>
<?php

if(isset($_GET["id"]) && trim($_GET["id"]) ) {
  $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
  $resultTela = mysqli_query($con,'SELECT nm_tela, ds_arquivo, cd_tela
                                    FROM tela where `cd_tela` = '.$_GET["id"]);
  $row = mysqli_fetch_array($resultTela);
  echo '<script type="text/javascript">'
     , 'setTextCampos("'. $row['nm_tela'] .'",
                          "'. $row['ds_arquivo'] .'");'
      , '</script>';
  mysqli_close($con);

}
	require_once('footer.php');
?>
