<?php

  $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
  if (mysqli_connect_errno()) {
    $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
    mysqli_close($con);
    exit;
  }

  $dataSet = array();
  $resultPer = mysqli_query($con,"SELECT * FROM PRODUTO_VENDIDO LIMIT 10");
  array_push($dataSet, array("Produto", "Quantidade Alugada"));
  while($row = mysqli_fetch_array($resultPer)) {
    array_push($dataSet, array('Cod: ' . $row['cd_produto'] . ' - ' . $row['nm_produto'], (int)$row['Qtd']) );
  }
  mysqli_close($con);
  echo json_encode($dataSet);
?>
