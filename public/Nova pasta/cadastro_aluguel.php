<?php
  $nome_pag = "Cadastro de Aluguel";
  require_once('aluguelController.php');
	require_once('header.php');


  $permissaoVer = false;
  $_SESSION['permissaoCadastrar'] = false;

  foreach ($_SESSION['Permissoes'] as $key => $value) {
    if(!is_array($value)) {
        if(stristr($nome_pag, $value)) {
          $arrayPerm = $_SESSION['Permissoes'][$value+1];

          if($arrayPerm['visualizar'] == "S") {
            $permissaoVer = true;
          }

          if($arrayPerm['cadastrar'] == "S") {
            $_SESSION['permissaoCadastrar'] = true;
          }
          break;
        }
    }
  }

  if(!$permissaoVer) {
    $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
    header("Location: aluguel"); exit;
  }

?>

<script type="text/javascript">
  var date = new Date()
  var dateStr = date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
  var arrayProdutos = [];
  $("#dtAluguel").html(dateStr);
</script>

<div class="container">

  <!-- Modal selecionar cliente -->
  <div class="modal fade" id="modalCliente" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Selecione o cliente</h4>
          </div>

          <table id="tabelaClientes" class="table table-hover">
             <thead>
               <tr>
                 <th>Nome</th>
                 <th>CPF</th>
                 <th>Ações</th>
               </tr>
             </thead>
             <tbody>
                <?php
                    $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
                    if (mysqli_connect_errno()) {
                      $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
                    }
                    $resultPer = mysqli_query($con,"SELECT cd_cliente, nm_cliente, nr_cpf
                                                      FROM cliente");
                    while($row = mysqli_fetch_array($resultPer)) {
                      echo '  <tr>
                                <td>' . $row['nm_cliente'] . '</td>
                                <td>' . $row['nr_cpf'] . '</td>
                                <td>
                                  <button class="btn btn-info btn-sm" onclick="selecionaCliente(' . $row['cd_cliente'] . ', \'' . $row['nm_cliente'] . '\')"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                                </td>
                            </tr>';
                    }
                    mysqli_close($con);
                ?>
             </tbody>
           </table>


          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          </div>
        </div>

      </div>
    </div>

  <div class="modal fade" id="modalProduto" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Selecione o produto</h4>
        </div>
        <table id="tabelaProdutos" class="table table-hover">
          <thead>
            <tr>
              <th>Nome</th>
              <th>Categoria</th>
              <th>Tamanho</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
            <?php
                $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
                if (mysqli_connect_errno()) {
                  $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
                  mysqli_close($con);
                }

                $resultPer = mysqli_query($con,"SELECT *
                                                  FROM produto p, categoria c
                                                 WHERE p.cd_categoria = c.cd_categoria");
                while($row = mysqli_fetch_array($resultPer)) {
                  echo '  <tr>
                            <td>' . $row['nm_produto'] . '</td>
                            <td>' . $row['nm_categoria'] . '</td>
                            <td>' . $row['ds_tamanho'] . '</td>
                            <td>
                              <button class="btn btn-info btn-sm" onclick="selecionaProduto(' . $row['cd_produto'] . ', \'' . $row['nm_produto'] . '\', ' . intval($row['vl_aluguel']) . ')"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                            </td>
                        </tr>';
                }
                mysqli_close($con);
            ?>
          </tbody>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>

  <form class="form-horizontal" onsubmit="organizaProdutos()" role="form" action="" method="post">
    <fieldset>
        <h3>Informações</h3>
          <div class="form-group">
            <div class="col-xs-2">
                <label for="cliente">Data do aluguel</label>
                <p id="dtAluguel" class="form-control-static"></p>
            </div>
            <div class="col-xs-1" style="display: none;">
                <label>Código</label>
                  <input type="text" class="form-control" id="codigoCliente" name="codigoCliente">
              </div>
            <div class="col-xs-6">
              <label for="nomeCliente">Cliente</label>
              <div class="input-group">
                <input type="text" id="nomeCliente" class="form-control" placeholder="Cliente" disabled="true">
                <div class="input-group-btn">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modalCliente"><i class="fa fa-search"></i></button>
                    </span>
                </div>
              </div>
            </div>

            <div class="col-xs-4">
              <label for="atendente">Atendente</label>
              <div class="col-sm-10">
                <select class="form-control" name="sltFuncionario" id="sel2">
                  <?php
                    $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
                    if (mysqli_connect_errno()) {
                        $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
                        mysqli_close($con);

                    }
                    $resultPer = mysqli_query($con,"SELECT cd_atendente, nm_atendente
                                                      FROM atendente");
                    while($row = mysqli_fetch_array($resultPer)) {
                        echo '<option value="' . $row['cd_atendente'] . '">' . $row['nm_atendente'] . '</option>';
                    }

                    mysqli_close($con);
                  ?>
                </select>
              </div>
            </div>
          </div>

        <h3>Datas</h3>
          <div class="form-group">
            <div class="col-xs-4">
                <label for="dataProva">Data de Prova</label>
                <input id="dataProva" name="dataProva" class="form-control" type="date" placeholder="Data de prova">
            </div>
            <div class="col-xs-4">
                <label for="dataRetirada">Data de Retirada</label>
                <input id="dataRetirada" name="dataRetirada" class="form-control" type="date" placeholder="Data de retirada">
            </div>
            <div class="col-xs-4">
                <label for="dataDevolucao">Data de Devolução</label>
                <input id="dataDevolucao" name="dataDevolucao" class="form-control" type="date" placeholder="Telefone de devolução">
            </div>
          </div>

        <h3>Produtos</h3>
          <div class="form-group">
            <div class="col-xs-1" style="display: none;">
                <label>Código</label>
                <input type="text" class="form-control" id="codigoProduto">
            </div>
            <div class="col-xs-4">
              <label for="nomeProduto">Produto</label>
              <div class="input-group">
                <input type="text" id="nomeProduto" class="form-control" placeholder="Produto" disabled="true">
                <div class="input-group-btn">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modalProduto"><i class="fa fa-search"></i></button>
                    </span>
                </div>
              </div>
            </div>
            <div class="col-xs-2">
                <label for="valorSugerido">Valor sugerido</label>
                <div class="input-group">
                  <span class="input-group-addon">R$</span>
                  <input id="valorSugerido" name="valorSugerido" class="form-control" type="text" disabled="true">
                  <span class="input-group-addon">,00</span>
                </div>
            </div>
            <div class="col-xs-2">
                <label for="valorAluguel">Valor do aluguel</label>
                <div class="input-group">
                  <span class="input-group-addon">R$</span>
                  <input id="valorAluguel" name="valorAluguel" class="form-control" type="text">
                  <span class="input-group-addon">,00</span>
                </div>
            </div>

            <div class="col-xs-2">
              <label style="visibility: hidden;">adicionar</label>
              <div onclick="adicionarClick()" class="btn btn-success btn-primary btn-block"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar</div>
            </div>
            <div id="tabpagerProduto" class="col-xs-12" style="margin-top: 20px;">
            <table id="tabelaItensAluguel" class="table table-hover">
               <thead>
                 <tr>
                   <th>Produto</th>
                   <th>Valor Sugerido</th>
                   <th>Valor do Aluguel</th>
                   <th>Ações</th>
                 </tr>
               </thead>
               <tbody id="conteudoTabelaItensAluguel">
                <?php
                  if(isset($_GET["id"]) && trim($_GET["id"]) ) {
                    $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
                    if (mysqli_connect_errno()) {
                      $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
                      mysqli_close($con);
                      exit;
                    }

                    $result = mysqli_query($con,"SELECT p.cd_produto, p.nm_produto, p.vl_aluguel, ia.vl_item
                                                  FROM item_aluguel ia, produto p
                                                 WHERE ia.cd_produto = p.cd_produto
                                                   AND ia.cd_aluguel = '" . $_GET["id"] . "';");
                    while($row = mysqli_fetch_array($result)) {
                      echo '<tr codigoProduto="' . $row['cd_produto'] . '">
                              <td>' . $row['nm_produto'] . '</td>
                              <td>' . str_replace(".",",",$row['vl_aluguel']) . '</td>
                              <td>' . str_replace(".",",",$row['vl_item']) . '</td>
                              <td><button class="btn btn-danger btn-sm" onclick="removeLinha(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                            </tr><script>arrayProdutos.push({codigo: ' . $row['cd_produto'] . ', preco: parseInt(' . str_replace(".",",",$row['vl_aluguel']) . ')});</script>';
                    }

                  }
                ?>
               </tbody>
             </table>
             <div class="pager">
                  <span class="page-number clickable active">1</span>
                </div>
           </div>
           <input type="text" name="produtosSelecionados" class="form-control" id="produtosSelecionados" style="visibility: hidden">
          </div>
          <h3>Valores</h3>
            <div class="form-group">
              <div class="col-xs-2">
                <label for="preco_total">Valor total:</label>
                <h4 id="preco_total">R$ 0,00</h4>
                <input id="inptPrecoTotal" name="inptPrecoTotal" class="form-control" type="text" style="visibility: hidden">
              </div>
              <div class="col-xs-3">
                <label for="valorPago">Valor pago</label>
                <div class="input-group">
                  <span class="input-group-addon">R$</span>
                  <input id="valorPago" name="valorPago" class="form-control" type="text">
                  <span class="input-group-addon">,00</span>
                </div>
              </div>
            </div>
          <button type="submit" class="btn btn-success">Salvar</button>
          <br/>
          <br/>
          <br/>
    </fieldset>
  </form>
</div>
<script type="text/javascript">
function organizaProdutos(){
    var arrayObj = '[';
    $('#conteudoTabelaItensAluguel tr').each(function(){
      if(arrayObj != '[')
        arrayObj += ',';

      arrayObj += '{';
      arrayObj += '"codigo":"'+ $(this).attr('codigoProduto') + '",';
      arrayObj += '"preco":"' + $(this).children()[2].innerText.split(',')[0] + '"';
      arrayObj += '}';
    });
    arrayObj += ']';

    $('#produtosSelecionados').val(arrayObj.toString());
  }


function removeLinha(linha) {
  var cod = $(linha).parent().parent().attr('codigoProduto')
  for (var i = 0; i < arrayProdutos.length; i++) {
    if(arrayProdutos[i].codigo == cod) {
      arrayProdutos.splice(i, 1);
    }
  }

  calculaPrecoTotal();
    $(linha).parent().parent().remove();
    $('#tabelaItensAluguel').trigger('repaginar');
  }

function selecionaCliente(cd, nome) {
  $("#nomeCliente").val(nome);
  $("#codigoCliente").val(cd);
  $("#modalCliente").modal("hide");
 }

 function selecionaProduto(cd, nome, precoSugerido) {

   $("#nomeProduto").val(nome);
   $("#codigoProduto").val(cd);
   $("#valorSugerido").val(precoSugerido);
   $("#modalProduto").modal("hide");
  }

  var currentPage = 0;
  var numRows = 0;
  $(document).ready(function(){
    desenhaTodasTabelas();
  });

  function desenhaTodasTabelas() {
    desenhaPaginacao('#tabelaItensAluguel');
  }

  function desenhaPaginacao(idTabela) {
    // Adaptado de:  http://gabrieleromanato.name/jquery-easy-table-pagination/
    $(idTabela).each(function() {
          currentPage = 0;
          var numPerPage = 6;
          var $table = $(this);
          $table.bind('trocarPag', function() {
              $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        $table.bind('repaginar', function() {
          var numRowsNew = $table.find('tbody tr').length;
          var numPages = Math.ceil(numRowsNew / numPerPage);

          if (numRows < numRowsNew && currentPage < numPages-1)
            currentPage++;
          else if (numRows > numRowsNew && currentPage > numPages-1)
            currentPage--;

              numRows = numRowsNew;

              $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();

          var $pager = $('<div class="pager"></div>');
          for (var page = 0; page < numPages; page++) {
                $('<span class="page-number"></span>').text(page + 1).bind('click', {
                  newPage: page
              }, function(event) {
                    currentPage = event.data['newPage'];
                  $table.trigger('trocarPag');
                  $(this).addClass('active').siblings().removeClass('active');
              }).appendTo($pager).addClass('clickable').attr('id', 'pager-produto-'+page);
          }

          $('#tabpagerProduto .pager').replaceWith($pager);
          $('#tabpagerProduto .pager #pager-produto-'+currentPage).addClass('active').siblings().removeClass('active');
          $table.trigger('trocarPag');
        });
      });
  }

  function adicionarClick() {
    if ($('#nomeProduto').val() == '' || $('#valorSugerido').val() == '' || $('#valorAluguel').val() == '') {
        return;
    }

    var produto = {codigo: $('#codigoProduto').val(), preco: parseInt($('#valorAluguel').val())};
    arrayProdutos.push(produto);

    $('#conteudoTabelaItensAluguel').append(''
      +'<tr codigoProduto="' + produto.codigo + '">'
        +'<td>' + $('#nomeProduto').val() + '</td>'
        +'<td>' + $('#valorSugerido').val() + ',00</td>'
        +'<td>' + produto.preco + ',00</td>'
        +'<td><button type="button" class="btn btn-danger btn-sm" onclick="removeLinha(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></td>'
      +'</tr>');

    $("#nomeProduto").val('');
    $("#codigoProduto").val('');
    $("#valorSugerido").val('');
    $("#valorAluguel").val('');

    $('#tabelaItensAluguel').trigger('repaginar');

    calculaPrecoTotal();
    }

    function calculaPrecoTotal() {
      var vlrTotal = 0;
      for (var i = 0; i < arrayProdutos.length; i++) {
        vlrTotal += parseInt(arrayProdutos[i].preco);
      }
      $("#preco_total").text('R$ ' + vlrTotal + ',00');
      $("#inptPrecoTotal").val(vlrTotal);

    }

  function setTextoNosCampos(dt_aluguel, cd_cliente, nm_cliente, cd_atendente, dt_prova, dt_retirada, dt_devolucao, vl_pago) {
    $('#dtAluguel').text(dt_aluguel.split('-')[2]+'/'+dt_aluguel.split('-')[1]+'/'+dt_aluguel.split('-')[0]);
    $('#codigoCliente').val(cd_cliente);
    $('#nomeCliente').val(nm_cliente);
    $('#sel2').val(cd_atendente);
    $('#dataProva').val(dt_prova);
    $('#dataRetirada').val(dt_retirada);
    $('#dataDevolucao').val(dt_devolucao);
    $('#valorPago').val(vl_pago.split('.')[0]);
    calculaPrecoTotal();
  }
</script>

<?php

  $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');

  if(isset($_GET["id"]) && trim($_GET["id"]) ) {
    $resultFunc = mysqli_query($con,'SELECT a.dt_aluguel, c.cd_cliente, c.nm_cliente, aten.cd_atendente, a.dt_prova, a.dt_retirada, a.dt_devolucao, a.vl_pago FROM aluguel a, cliente c, atendente aten WHERE a.cd_cliente = c.cd_cliente AND a.cd_atendente = aten.cd_atendente AND a.cd_aluguel = '.$_GET["id"]);
    $row = mysqli_fetch_array($resultFunc);

    echo '<script type="text/javascript">'
       , 'setTextoNosCampos("'. $row['dt_aluguel'] .'",
                            "'. $row['cd_cliente'] .'",
                            "'. $row['nm_cliente'] .'",
                            "'. $row['cd_atendente'] .'",
                            "'. $row['dt_prova'] .'",
                            "'. $row['dt_retirada'] .'",
                            "'. $row['dt_devolucao'] .'",
                            "'. $row['vl_pago'] .'");'
        , '</script>';
  }
  mysqli_close($con);
	require_once('footer.php');
?>
