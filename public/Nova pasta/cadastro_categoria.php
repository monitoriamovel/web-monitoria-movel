<?php
	require_once('categoriaController.php');
	$nome_pag = "Cadastro de Categoria";
	require_once('header.php');

	$permissaoVer = false;
	$_SESSION['permissaoCadastrar'] = false;

	foreach ($_SESSION['Permissoes'] as $key => $value) {
		if(!is_array($value)) {
				if(stristr($nome_pag, $value)) {
					$arrayPerm = $_SESSION['Permissoes'][$value+1];

					if($arrayPerm['visualizar'] == "S") {
						$permissaoVer = true;
					}

					if($arrayPerm['cadastrar'] == "S") {
						$_SESSION['permissaoCadastrar'] = true;
					}
					break;
				}
		}
	}

	if(!$permissaoVer){
		$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
		header("Location: produto"); exit;
	}
?>

<div class="container">
	<form class="form-horizontal" role="form" action="" method="post">
        <fieldset>
		    <div class="form-group">
			    <div class="col-xs-5">
			        <label for="descricao">Descrição da Categoria</label>
			        <input id="descricao" autofocus="" required="" name="descricao" class="form-control" type="textarea" placeholder="Descrição">
			    </div>
		    </div>
			<button type="submit" class="btn btn-success">Cadastrar</button>
		</fieldset>
	</form>
</div>


<script type="text/javascript">
  function setTextDescricao(descricao) {
    document.getElementById('descricao').value = descricao;
  }
</script>


<?php

//iasset verifica se a variavel existe e se tem vai setar o texto no campo correto
  if(isset($_GET["id"]) && trim($_GET["id"]) ) {
		$con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
    $resultPer = mysqli_query($con,'SELECT nm_categoria, cd_categoria from categoria where cd_categoria = '.$_GET["id"]);
    $row = mysqli_fetch_array($resultPer);
    $resultStr = (string) $row[0];
    echo '<script type="text/javascript">'
       , 'setTextDescricao("'. $resultStr .'");'
       , '</script>';
    mysqli_close($con);

  }

	require_once('footer.php');
?>
