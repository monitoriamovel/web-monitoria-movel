<?php

	///////////////////////////////
	///			CENTRAL 		///
	///////////////////////////////

	function postPersist(){

		if($_SESSION['permissaoCadastrar'] == false || $_SESSION['permissaoCadastrar'] < 1) {
		  $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
		  header("Location: cadastro_aluguel");
		  exit;
		}

		$con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
	    if (mysqli_connect_errno()) {
	      $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
	      mysqli_close($con);
	      exit;
	    }

		if(empty($_POST['codigoCliente']) OR empty($_POST['sltFuncionario']) OR empty($_POST['dataProva']) OR empty($_POST['codigoCliente']) OR empty($_POST['codigoCliente']) OR empty($_POST['codigoCliente']) OR empty($_POST['codigoCliente'])){
			$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Favor informar os dados corretamente!</div>');
			mysqli_close($con);
		} else {

			$produtos = json_decode($_POST['produtosSelecionados']);

			if(isset($_GET["id"]) && trim($_GET["id"]) ) {
			  //update
				$msg = 'Alterado';

				$queryAlguel = "UPDATE aluguel SET
					cd_cliente = '" . $_POST['codigoCliente'] . "',
					cd_atendente = '" . $_POST['sltFuncionario'] . "',
					vl_pago = '" . $_POST['valorPago'] . "',
					vl_total = '" . $_POST['inptPrecoTotal'] . "',
					dt_prova = '" . $_POST['dataProva'] . "',
					dt_retirada = '" . $_POST['dataRetirada'] . "',
					dt_devolucao = '" . $_POST['dataDevolucao'] . "',
					ds_observacao = ''
					WHERE cd_aluguel = " . $_GET["id"];

				if(mysqli_query($con, $queryAlguel)) {

					if(mysqli_query($con,"DELETE FROM item_aluguel WHERE cd_aluguel = " . $_GET["id"])) {

						foreach ( $produtos as $produto ) {
							$queryProduto = "INSERT INTO item_aluguel (cd_aluguel, cd_produto, ds_observacao, vl_item) VALUES
							( " . $_GET["id"] .",
							". $produto->codigo . ",
							'',
							". $produto->preco . "
							);";

							if(!mysqli_query($con, $queryProduto)) {

									$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Ocorreu um erro ao salvar. ' . mysqli_error($con) . '</div>');
									mysqli_query($con, "ROLLBACK");
									mysqli_close($con);
							}
			    		}
			    		if (!isset($_SESSION['alertMessage'])) {
			    			$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-success" role="alert">'. $msg . ' com sucesso!</div>');
							mysqli_query($con, "COMMIT");
							header("Location: aluguel");
							mysqli_close($con);
							exit;
			    		}

			    	} else {
						$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Ocorreu um erro ao salvar. ' . mysqli_error($con) . '</div>');
						mysqli_query($con, "ROLLBACK");
						mysqli_close($con);
					}
				} else {
					$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Ocorreu um erro ao salvar. ' . mysqli_error($con) . '</div>');
					mysqli_query($con, "ROLLBACK");
					mysqli_close($con);
				}

			} else {
			  //insert
				$msg = 'Cadastrado';

			  	$queryAlguel = "INSERT INTO aluguel(cd_cliente, cd_atendente, vl_pago, vl_total, dt_aluguel ,dt_prova, dt_retirada, dt_devolucao, ds_observacao) VALUES
				('" . $_POST['codigoCliente'] . "',
				'" . $_POST['sltFuncionario'] . "',
				'" . $_POST['valorPago'] . "',
				'" . $_POST['inptPrecoTotal'] . "',
				'" . date("Y-m-d") . "',
				'" . $_POST['dataProva'] . "',
				'" . $_POST['dataRetirada'] . "',
				'" . $_POST['dataDevolucao'] . "',
				''
				);";

				if(mysqli_query($con, $queryAlguel)) {

					$cod_aluguel = mysqli_insert_id($con);

					foreach ( $produtos as $produto ) {
						$queryProduto = "INSERT INTO item_aluguel (cd_aluguel, cd_produto, ds_observacao, vl_item) VALUES
						( " . $cod_aluguel .",
						". $produto->codigo . ",
						'',
						". $produto->preco . "
						);";

						if(!mysqli_query($con, $queryProduto)) {

								$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Ocorreu um erro ao salvar. ' . mysqli_error($con) . '</div>');
								mysqli_query($con, "ROLLBACK");
								mysqli_close($con);
						}
		    		}
		    		if (!isset($_SESSION['alertMessage'])) {
		    			$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-success" role="alert">'. $msg . ' com sucesso!</div>');
						mysqli_query($con, "COMMIT");
						header("Location: aluguel");
						mysqli_close($con);
						exit;
		    		}
				} else {
					$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Ocorreu um erro ao salvar. ' . mysqli_error($con) . '</div>');
					mysqli_query($con, "ROLLBACK");
					mysqli_close($con);
				}
			}
		}
	}
?>
