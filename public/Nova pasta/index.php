<?php
	require_once('valida_usuario.php');

	if (isset($_SESSION['FuncionarioID'])) {
		header("Location: home");
	}

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		validaUsuario();
	}
?>

<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login - SLT</title>

    <!-- Bootstrap -->
    <link href="src/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      
    <style type="text/css" media="all">
        body {
            background-color: #daefff;
        }
        #logo {
            width: 100%;
        }
        .form-signin {
            position: absolute;
            width: 30%;
            height: 30%;
            top: 15%;
            left: 35%;
        }
        .form-signin-heading {
            text-align: center;
        }
        .form-signin * {
            margin: 2% auto;
        }
        .btn-logar {
            width: 100%;
        }
    </style>
  </head>
  <body>
    
    
    <div class="container">

      <form class="form-signin" action="" method="post">
        <h2 class="form-signin-heading"><img id="logo" src="src/img/logoNav.png"></h2>
        <div id="msg-retorno">
        <?php
            if (isset($_SESSION['alertMessage'])){
            	echo utf8_encode($_SESSION['alertMessage']);
            	session_destroy();
            }
        ?>
        </div>
        <input class="form-control" autofocus="" required="" type="text" placeholder="Usuário" name="usuario">
        <input class="form-control" required="" type="password" placeholder="Senha" name="senha">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Logar</button>
      </form>

    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="src/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="src/js/bootstrap.min.js"></script>
  </body>
</html>