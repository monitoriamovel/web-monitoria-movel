<?php
	$nome_pag = "Relatorios";
	require_once ('header.php');
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<script type="text/javascript">

 google.charts.load('current', {'packages':['corechart']});
 google.charts.setOnLoadCallback(drawChart);

 function drawChart() {
	 var jsonData = $.ajax({
       url: "getRelatorioProdutosMaisAlugados.php",
       dataType: "json",
       async: false
       }).responseText;

	 var jsonData2 = $.ajax({
	     url: "getFaturamentoMensal.php",
	     dataType: "json",
	     async: false
	     }).responseText;

	 var jsonData3 = $.ajax({
				url: "getFaturamentoFuncionarioUltimoMes.php",
				dataType: "json",
				async: false
				}).responseText;

	 var data = new google.visualization.arrayToDataTable(JSON.parse(jsonData));
	 var data2 = new google.visualization.arrayToDataTable(JSON.parse(jsonData2));
	 var data3 = new google.visualization.arrayToDataTable(JSON.parse(jsonData3));

	 var options1 = {
     width: 400,
     height: 300,
     chartArea:{left:10,top:10,width:"95%",height:"95%"},
   };

	 var options2 = {
     width: 400,
     height: 300,
		 hAxis: {
			title: 'Mes'
			},
		vAxis: {
			title: 'Valor'
		},
   };

	 var chart = new google.visualization.PieChart(document.getElementById('produtosMaisAlugados'));
	 var chart2 = new google.visualization.LineChart(document.getElementById('faturamentoMensal'));
	 var chart3 = new google.visualization.PieChart(document.getElementById('valorAlugueisFuncionario'));
	 chart.draw(data, options1);
	 chart2.draw(data2, options2);
	 chart3.draw(data3, options1);

 }

 </script>

	<div class="container">
		<div class="col-xs-5">

		<h3>Os 10 produtos mais alugados</h3>
		 <div id="produtosMaisAlugados"></div>
	 </div class="col-xs-5">
 <div class="col-xs-5">

		 <h3>Faturalmento mensal dos ultimos 12 meses</h3>
		 <div id="faturamentoMensal"></div>
	 </div class="col-xs-5">
	 <div class="col-xs-5">

		 <h3>Faturamento dos ultimos 30 dias por funcionario</h3>
		 <div id="valorAlugueisFuncionario"></div>
	 </div class="col-xs-5">

	</div>


<?php




require_once ('footer.php');
?>
