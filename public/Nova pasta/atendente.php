<?php
    $nome_pag = "Atendente";
	  require_once('header.php');

    $permissaoVer = false;
    $_SESSION['permissaoCadastrar'] = false;

    foreach ($_SESSION['Permissoes'] as $key => $value) {
      if(!is_array($value)) {
          if(stristr($nome_pag, $value)) {
            $arrayPerm = $_SESSION['Permissoes'][$value+1];

            if($arrayPerm['visualizar'] == "S") {
              $permissaoVer = true;
            }

            break;
          }
      }
    }

    if(!$permissaoVer) {
      $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
      header("Location: home"); exit;
    }

?>

<div class="container">

  <div class="row">
    <div class="col-md-1 col-md-offset-10">
      <a href="cadastro_atendente" class="btn btn-success btn-primary btn-block" style="padding-right: 125px;"><i class="fa fa-plus" aria-hidden="true"></i> Novo Atendente</a>
      <a href="cadastro_permissao" class="btn btn-success btn-primary btn-block" style="padding-right: 125px;"><i class="fa fa-plus" aria-hidden="true"></i> Nova Permissão</a>
      <a href="cadastro_tela" class="btn btn-success btn-primary btn-block" style="padding-right: 125px;"><i class="fa fa-plus" aria-hidden="true"></i> Nova Tela</a>
    </div>
  </div>

  <h3>Atendentes</h3>
  <table id="tabelaAtendentes" class="table table-hover">
     <thead>
       <tr>
         <th scope="col">Nome</th>
         <th scope="col">CPF</th>
         <th scope="col">Telefone residêncial</th>
         <th scope="col">Telefone celular</th>
         <th scope="col">Permissões</th>
         <th scope="col">Ações</th>
       </tr>
     </thead>
     <tbody>
       <?php
          $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
          if (mysqli_connect_errno()) {
            $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
            mysqli_close($con);
            exit;
          }
          $resultFun = mysqli_query($con,'SELECT cd_atendente, nm_atendente, nr_cpf, nr_telefone_casa, nr_telefone_celular
                                            FROM atendente');
          while($row = mysqli_fetch_array($resultFun)) {
            $permissoes = '';
            $resultPer = mysqli_query($con,"SELECT p.nm_permissao
                                              FROM atendente_permissao ap, permissao p
                                             WHERE ap.cd_permissao = p.cd_permissao
                                               AND ap.cd_atendente = " . $row['cd_atendente'] . "
                                             GROUP BY p.nm_permissao");
            while($row2 = mysqli_fetch_array($resultPer)) {
              if ($permissoes == '')
                $permissoes .= $row2['nm_permissao'];
              else
                $permissoes .= ', ' . $row2['nm_permissao'];
            }

            echo '<tr>
                    <td>' . $row['nm_atendente'] . '</td>
                    <td>' . Mask('###.###.###-##', $row['nr_cpf']) . '</td>
                    <td>' . Mask(((strlen($row['nr_telefone_casa']) == 11) ? '(##) #####-####' : '(##) ####-####'), $row['nr_telefone_casa']) . '</td>
                    <td>' . Mask(((strlen($row['nr_telefone_celular']) == 11) ? '(##) #####-####' : '(##) ####-####'), $row['nr_telefone_celular']) . '</td>
                    <td>' . $permissoes . '</td>
                    <td>
                    <button class="btn btn-warning btn-sm" onclick="editarAtendente('. $row['cd_atendente'] . ')"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    </td>
                  </tr>';
          }

          mysqli_close($con);
       ?>
     </tbody>
   </table>

   <h3>Permissões</h3>
   <table id="tabelaPermissoes" class="table table-hover">
     <thead>
       <tr>
         <th scope="col">Descrição</th>
         <th scope="col">Ação</th>
       </tr>
     </thead>
     <tbody>
        <?php
          $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
          if (mysqli_connect_errno()) {
            $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
            mysqli_close($con);
            exit;
          }

          $resultPer = mysqli_query($con,"SELECT nm_permissao, cd_permissao
                                            FROM permissao");
          while($row = mysqli_fetch_array($resultPer)) {
            echo '<tr>
                   <td>' . $row['nm_permissao'] . '</td>
                   <td>
                   <button class="btn btn-warning btn-sm" onclick="editarPermissao('. $row['cd_permissao'] . ')"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                   </td>
                 </tr>';
          }
          mysqli_close($con);
        ?>
     </tbody>
   </table>

   <h3>Tela</h3>
   <table id="tabelaTela" class="table table-hover">
     <thead>
       <tr>
         <th scope="col">Nome</th>
         <th scope="col">Nome do Arquivo</th>
         <th scope="col">Ação</th>
       </tr>
     </thead>
     <tbody>
        <?php
          $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
          if (mysqli_connect_errno()) {
            $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
            mysqli_close($con);
            exit;
          }

          $resultPer = mysqli_query($con,"SELECT t.nm_tela, t.ds_arquivo, t.cd_tela
                                            FROM tela t");
          while($row = mysqli_fetch_array($resultPer)) {
            echo '<tr>
                   <td>' . $row['nm_tela'] . '</td>
                   <td>' . $row['ds_arquivo'] . '</td>
                   <td>
                   <button class="btn btn-warning btn-sm" onclick="editarTela('. $row['cd_tela'] . ')"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                   </td>
                 </tr>';
          }
          mysqli_close($con);
        ?>
     </tbody>
   </table>

</div>

<script type="text/javascript">

function editarPermissao(cd_permissao) {
  location.href = './cadastro_permissao?id=' + cd_permissao;
}

function editarAtendente(cd_atendente) {
  location.href = './cadastro_atendente?id=' + cd_atendente;
}
function editarTela(cd_atendente) {
  location.href = './cadastro_tela?id=' + cd_atendente;
}

function removerCelula(cel) {
 var p=cel.parentNode.parentNode;
     p.parentNode.removeChild(p);
     $('#tabelaAtendentes').trigger('repaginate');
     $('#tabelaPermissoes').trigger('repaginate');
     $('#tabelaTela').trigger('repaginate');
}

  $(document).ready(function(){
    desenhaTodasTabelas();
  });

  function desenhaTodasTabelas() {
    desenhaPaginacao('#tabelaAtendentes');
    desenhaPaginacao('#tabelaPermissoes');
    desenhaPaginacao('#tabelaTela');
  }

  function desenhaPaginacao(idTabela) {
    // Adaptado de:  http://gabrieleromanato.name/jquery-easy-table-pagination/
    $(idTabela).each(function() {
      var currentPage = 0;
      var numPerPage = 6;
      var $table = $(this);
      $table.bind('repaginate', function() {
          $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
      });
      $table.trigger('repaginate');
      var numRows = $table.find('tbody tr').length;
      var numPages = Math.ceil(numRows / numPerPage);
      var $pager = $('<div class="pager"></div>');
      for (var page = 0; page < numPages; page++) {
          $('<span class="page-number"></span>').text(page + 1).bind('click', {
              newPage: page
          }, function(event) {
              currentPage = event.data['newPage'];
              $table.trigger('repaginate');
              $(this).addClass('active').siblings().removeClass('active');
          }).appendTo($pager).addClass('clickable');
      }
      $pager.insertAfter($table).find('span.page-number:first').addClass('active');
    });
  }
</script>
<?php
	require_once('footer.php');
?>
