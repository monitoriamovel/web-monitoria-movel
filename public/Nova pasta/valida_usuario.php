<?php

function validaUsuario() {
	// Se a sess�o n�o existir, inicia uma
	if (!isset($_SESSION)) session_start();

	// Verifica se houve POST e se o usu�rio ou a senha �(s�o) vazio(s)
	if (empty($_POST['usuario']) OR empty($_POST['senha'])) {
		$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Favor informar o login corretamente!</div>');
	}

	// Tenta se conectar ao servidor MySQL
	$con = mysqli_connect('localhost', 'root', '', 'trafnprog3');

	if (mysqli_connect_errno()) {
		$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
	}

	$usuario = mysqli_real_escape_string($con, $_POST['usuario']);
	$senha = mysqli_real_escape_string($con, $_POST['senha']);

	// Valida��o do usu�rio/senha digitados
	$sql = "SELECT `cd_atendente`, `nm_atendente` FROM `atendente` WHERE (`ds_login` = '". $usuario .
		   "') AND (`ds_senha` = '". $senha ."') LIMIT 1";

	$query = mysqli_query($con, $sql);

	if (mysqli_num_rows($query) != 1) {
		$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Login inválido!</div>');
	} else {
		// Salva os dados encontados na vari�vel $resultado
		$resultado = mysqli_fetch_assoc($query);

		// Salva os dados encontrados na sess�o
		$temp_codAtendente = utf8_decode($resultado['cd_atendente']);
		$temp_nmAtendente = utf8_decode($resultado['nm_atendente']);

		mysqli_close($con);

		/////////// ADICIONA TODAS AS PERMISSOES QUE O FUNCIONARIO TEM EM UM ARRAY DO SESSION
		$con = mysqli_connect('localhost', 'root', '', 'trafnprog3');


		$sql = "SELECT t.nm_tela, tp.fl_visualizar, tp.fl_cadastrar FROM tela_permissao tp, tela t where cd_permissao = (select cd_permissao from atendente_permissao where cd_atendente = $temp_codAtendente limit 1) and t.cd_tela = tp.cd_tela;";
		$query = mysqli_query($con, $sql);

		if (mysqli_connect_errno()) {
			$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
		} else {
			$arrayPermissoes = array();
			$temResultado = false;
			while($row = mysqli_fetch_array($query)) {
				$permTela = array( 'visualizar'	=> $row['fl_visualizar'],
	              						 'cadastrar'	=> $row['fl_cadastrar']
	          	 							);
				array_push($arrayPermissoes, $row['nm_tela'], $permTela);
				$temResultado=true;
			}

			if(!$temResultado) {
				$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
			} else {
				$_SESSION['FuncionarioID'] = $temp_codAtendente;
				$_SESSION['FuncionarioNome'] = $temp_nmAtendente;
				$_SESSION['Permissoes'] = $arrayPermissoes;
				//adiciona o array
				// Redireciona o visitante se tem permissao
				header("Location: home"); exit;

			}
		}
		mysqli_close($con);
		//////////




	}
}
?>
