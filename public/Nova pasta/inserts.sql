insert into tela (nm_tela, ds_arquivo) values ('Home','home');
insert into permissao(nm_permissao) values ('Admin');
insert into tela_permissao(cd_tela,cd_permissao,fl_visualizar,fl_cadastrar) values (1,1,'S','S');
insert into endereco(ds_estado, ds_cidade, ds_bairro, nr_cep, ds_rua, nr_complemento, ds_complemento) values ('Santa Catarina', 'Blumenau', 'Itoupava Central','89068230','Carl Dettmer',249,'Casa');
insert into atendente(cd_endereco, nm_atendente, nr_cpf, nr_telefone_casa, nr_telefone_celular, ds_login, ds_senha) values (1,'Renan Fiedler de Oliveira','07031420970','4733372925','4788615451','renan','123');
insert into endereco(ds_estado, ds_cidade, ds_bairro, nr_cep, ds_rua, nr_complemento, ds_complemento) values ('Santa Catarina', 'Pomerode', 'Bairro de pomerode','12345678','Rua de Pomerode',123,'Apartamento');
insert into cliente(cd_endereco, nm_cliente, dt_nascimento, nr_rg, nr_cpf) values (2,'Cliente','2000-01-01','5789789','12345678912');
insert into categoria(nm_categoria) values ('Vestido');
insert into produto(cd_categoria, nm_produto, vl_compra, vl_aluguel, ds_tamanho, dt_compra) values (1,'Vestido rosa', 1.10, 5,'M','2016-01-01');
insert into aluguel(cd_cliente,cd_atendente,vl_pago,vl_total,dt_prova,dt_retirada,dt_devolucao) values (1,1,1,1,'2016-06-20','2016-06-22','2016-06-27');
insert into aluguel(cd_cliente,cd_atendente,vl_pago,vl_total,dt_prova,dt_retirada,dt_devolucao) values (1,1,1,1,'2016-06-20','2016-06-22','2016-06-27');
insert into item_aluguel(cd_aluguel,cd_produto) values (1,1);
commit;

insert into item_aluguel(cd_aluguel,cd_produto) values (2,1);
commit;

select * from tela_permissao;
select * from atendente_permissao;

SELECT t.cd_tela, t.nm_tela, tp.fl_visualizar, tp.fl_cadastrar
                                              FROM tela_permissao tp, tela t
                                             WHERE tp.cd_tela = t.cd_tela
                                               AND tp.cd_permissao = 1