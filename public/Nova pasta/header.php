<?php
  // A sessão precisa ser iniciada em cada página diferente
  if (!isset($_SESSION)) session_start();
  
  // Verifica se não há a variável da sessão que identifica o usuário
  if (!isset($_SESSION['FuncionarioID'])) {
    // Destrói a sessão por segurança
    session_destroy();
    // Redireciona o visitante de volta pro login
    header("Location: "); exit;
  }

  // Método para chamar o controller para inserir
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    postPersist();
  }

  // Método para mascara
  function Mask($mask,$str){
    $str = str_replace(" ","",$str);

    for($i = 0; $i < strlen($str); $i++){
      $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;
  }
?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $nome_pag ?> - SLT</title>

    <script src="src/js/jquery.min.js"></script>
    <script src="src/js/jquery.maskedinput.js"></script>

    <link href="src/css/bootstrap.min.css" rel="stylesheet">
    <link href="src/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> -->

    <link href="src/css/style.css" rel="stylesheet">

  </head>
  <body>

    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <ul class="nav navbar-nav">
          <li><img id="logoNav" src="src/img/logoNav.png"></li>
          <li <?php if ($nome_pag == "Home") echo 'class="active"'; ?> ><a href="home">Home</a></li>
          <li role="presentation" class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Aluguel <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li <?php if ($nome_pag == "Aluguel") echo 'class="active"'; ?> ><a href="aluguel">Gerenciar</a></li>
              <li <?php if ($nome_pag == "Cadastro de Aluguel") echo 'class="active"'; ?> ><a href="cadastro_aluguel">Cadastrar</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cliente <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li <?php if ($nome_pag == "Cliente") echo 'class="active"'; ?> ><a href="cliente">Gerenciar</a></li>
              <li <?php if ($nome_pag == "Cadastro de Cliente") echo 'class="active"'; ?> ><a href="cadastro_cliente">Cadastrar</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Produto <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li <?php if ($nome_pag == "Produto") echo 'class="active"'; ?> ><a href="produto">Gerenciar</a></li>
              <li <?php if ($nome_pag == "Cadastro de Produto") echo 'class="active"'; ?> ><a href="cadastro_produto">Cadastrar Produto</a></li>
              <li <?php if ($nome_pag == "Cadastro de Categoria") echo 'class="active"'; ?> ><a href="cadastro_categoria">Cadastrar Categoria</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Atendentes <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li <?php if ($nome_pag == "Atendente") echo 'class="active"'; ?> ><a href="atendente">Gerenciar</a></li>
              <li <?php if ($nome_pag == "Cadastro de Atendente") echo 'class="active"'; ?> ><a href="cadastro_atendente">Cadastrar Atendente</a></li>
              <li <?php if ($nome_pag == "Cadastro de Permissão") echo 'class="active"'; ?> ><a href="cadastro_permissao">Cadastrar Permissão</a></li>
              <li <?php if ($nome_pag == "Cadastro de Tela") echo 'class="active"'; ?> ><a href="cadastro_tela">Cadastrar Tela</a></li>
            </ul>
          </li>
          <li <?php if ($nome_pag == "Relatorios") echo 'class="active"'; ?> ><a href="relatorios">Relatórios</a></li>

        </ul>
        <ul class='nav navbar-nav navbar-right'>
          <li><a href='logout' id='deslogar'><i class='fa fa-sign-out'></i> Deslogar</a></li>
        </ul>
      </div>
    </nav>

    <div class="container" style="margin-bottom: 10px;">
      <div id="msg-retorno">
      <?php
        if (isset($_SESSION['alertMessage'])){
          echo utf8_encode($_SESSION['alertMessage']);
          unset($_SESSION['alertMessage']);
        }
      ?>
      </div>
    </div>
