<?php
  $nome_pag = "Aluguel";
	require_once('header.php');

   $permissaoVer = false;
   $_SESSION['permissaoCadastrar'] = false;

   foreach ($_SESSION['Permissoes'] as $key => $value) {
     if(!is_array($value)) {
         if(stristr($nome_pag, $value)) {
           $arrayPerm = $_SESSION['Permissoes'][$value+1];

           if($arrayPerm['visualizar'] == "S") {
             $permissaoVer = true;
           }

           break;
         }
     }
   }

   if(!$permissaoVer) {
     $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
     header("Location: home"); exit;
   }

?>

<div class="container">

  <div class="row">
    <div class="col-md-1 col-md-offset-10">
      <a href="cadastro_aluguel" class="btn btn-success btn-primary btn-block btn-logo"><i class="fa fa-plus" aria-hidden="true"></i> Novo</a>
    </div>

  </div>

  <script type="text/javascript" src="src/js/calendar.js"></script>

  <!-- Modal selecionar cliente -->
  <div class="modal fade" id="modalAluguel" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Selecione o aluguel</h4>
          </div>

          <table id="tabelaAluguel" class="table table-hover">
             <thead>
               <tr>
                 <th>Cliente</th>
                 <th>Valor</th>
                 <th>Ações</th>
               </tr>
             </thead>
             <tbody id="tabelaItemAluguel">

             </tbody>
           </table>


          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          </div>
        </div>

      </div>
    </div>

  <ul class="pager">
    <li><a href="javascript:changeMonth(-1)" id="calendarMesButtons"><i class='fa fa-arrow-left'></i> Anterior</a></li>
    <li><h2 id="calendarMesTitulo" style="display: inline;"> aaaa - yyyy</h2></li>
    <li><a href="javascript:changeMonth(1)" id="calendarMesButtons">Próximo <i class='fa fa-arrow-right'></i></a></li>
  </ul>
  <table class="table table-bordered" style="background-color: white">
    <thead id="calendarioDiaTitulo">
      <tr>
        <th>Domingo</th>
        <th>Segunda-feira</th>
        <th>Terça-feira</th>
        <th>Quarta-feira</th>
        <th>Quinta-feira</th>
        <th>Sexta-feira</th>
        <th>Sábado</th>
      </tr>
    </thead>
    <tbody id="tabelaDias">

    </tbody>
    <script>defineTituloMes()</script>

  </table>

</div>

<script type="text/javascript">

function removerCelula(cel) {
 var p=cel.parentNode.parentNode;
     p.parentNode.removeChild(p);
     $('#tabelaAluguel').trigger('repaginate');
}

function editarAluguel(cd_aluguel) {
  location.href = './cadastro_aluguel?id=' + cd_aluguel;
}

  $(document).ready(function(){
    desenhaTodasTabelas();
  });

  function desenhaTodasTabelas() {
    desenhaPaginacao('#tabelaAluguel');
  }

  function desenhaPaginacao(idTabela) {
    // Adaptado de:  http://gabrieleromanato.name/jquery-easy-table-pagination/
    $(idTabela).each(function() {
      var currentPage = 0;
      var numPerPage = 6;
      var $table = $(this);
      $table.bind('repaginate', function() {
          $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
      });
      $table.trigger('repaginate');
      var numRows = $table.find('tbody tr').length;
      var numPages = Math.ceil(numRows / numPerPage);
      var $pager = $('<div class="pager"></div>');
      for (var page = 0; page < numPages; page++) {
          $('<span class="page-number"></span>').text(page + 1).bind('click', {
              newPage: page
          }, function(event) {
              currentPage = event.data['newPage'];
              $table.trigger('repaginate');
              $(this).addClass('active').siblings().removeClass('active');
          }).appendTo($pager).addClass('clickable');
      }
      $pager.insertAfter($table).find('span.page-number:first').addClass('active');
    });
  }
</script>

<?php
	require_once('footer.php');
?>
