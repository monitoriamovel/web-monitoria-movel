<?php
    require_once('atendenteController.php');
  $nome_pag = "Cadastro de Atendente";
	require_once('header.php');


  $permissaoVer = false;
  $_SESSION['permissaoCadastrar'] = false;

  foreach ($_SESSION['Permissoes'] as $key => $value) {
    if(!is_array($value)) {
        if(stristr($nome_pag, $value)) {
          $arrayPerm = $_SESSION['Permissoes'][$value+1];

          if($arrayPerm['visualizar'] == "S") {
            $permissaoVer = true;
          }

          if($arrayPerm['cadastrar'] == "S") {
            $_SESSION['permissaoCadastrar'] = true;
          }


          break;
        }
    }
  }

  if($permissaoVer == false) {
    $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
    header("Location: atendente"); exit;
  }


?>

<div class="container">
	<form class="form-horizontal" role="form" action="" method="post">
        <fieldset>
		    <div class="form-group">
			    <div class="col-xs-5">
			        <label for="nome">Nome Completo</label>
			        <input id="nome" name="nome" class="form-control" type="text" placeholder="Nome Completo">
			    </div>
			    <div class="col-xs-3">
			        <label for="cpf">CPF</label>
			        <input id="cpf" name="cpf" class="form-control" id="ex3" type="text" placeholder="CPF">
			    </div>
		    </div>

			<h3>Telefone</h3>
		    <div class="form-group">
			    <div class="col-xs-4">
			        <label for="telefoneCasa">Casa</label>
			        <input id="telefoneCasa" name="telefoneCasa" class="form-control" type="text" placeholder="Telefone Casa">
			    </div>
			    <div class="col-xs-4">
			        <label for="telefoneCelular">Celular</label>
			        <input id="telefoneCelular" name="telefoneCelular" class="form-control" type="text" placeholder="Telefone Celular">
			    </div>
		    </div>

			<h3>Endereço</h3>
            <div class="form-group">
                <div class="col-xs-3">
                    <label for="enderecoEstado">Estado</label>
                    <input id="enderecoEstado" required="" name="enderecoEstado" class="form-control" type="text" placeholder="Estado">
                </div>
                <div class="col-xs-3">
                    <label for="enderecoCidade">Cidade</label>
                    <input id="enderecoCidade" required="" name="enderecoCidade" class="form-control" type="text" placeholder="Cidade">
                </div>
                <div class="col-xs-3">
                    <label for="enderecoBairro">Bairro</label>
                    <input id="enderecoBairro" required="" name="enderecoBairro" class="form-control" type="text" placeholder="Rua">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-2">
                    <label for="enderecoCep">CEP</label>
                    <input id="enderecoCep" required="" name="enderecoCep" class="form-control" type="text" placeholder="CEP">
                </div>
                <div class="col-xs-4">
                    <label for="enderecoRua">Rua</label>
                    <input id="enderecoRua" required="" name="enderecoRua" class="form-control" type="text" placeholder="Rua">
                </div>
                <div class="col-xs-1">
                    <label for="numeroEndereco">Número</label>
                    <input id="numeroEndereco" required="" name="numeroEndereco" class="form-control" type="text" placeholder="Nº">
                </div>
                <div class="col-xs-2">
                    <label for="enderecoComplemento">Complemento</label>
                    <input id="enderecoComplemento" name="enderecoComplemento" class="form-control" type="text" placeholder="Comp.">
                </div>
            </div>

			<h3>Acesso</h3>
		    <div class="form-group">
			    <div class="col-xs-4">
			        <label for="usuario">Usuário</label>
			        <input name="usuario" id="usuario" class="form-control" type="text" placeholder="Usuário">
			    </div>
			    <div class="col-xs-4">
			        <label for="senha">Senha</label>
			        <input name="senha" id="senha" class="form-control" type="password" placeholder="Senha">
			    </div>
			    <div class="col-xs-4">
			        <label for="permissoes">Permissões</label>
					<div class="col-sm-10">
						<select multiple class="form-control" id="sel2" name="permissoes[]">
                            <?php
                                $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
                                if (mysqli_connect_errno()) {
                                  $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
                                  excepitonExit();
                                }
                                $resultPer = mysqli_query($con,"SELECT cd_permissao, nm_permissao
                                                                  FROM permissao p");
                                while($row = mysqli_fetch_array($resultPer)) {
                                    echo '<option value="' . $row['cd_permissao'] . '">' . $row['nm_permissao'] . '</option>';
                                }

                                mysqli_close($con);
                            ?>
						</select>
					</div>
			    </div>
		    </div>

			<button type="submit" class="btn btn-success">Cadastrar</button>
		</fieldset>
	</form>
</div>

<script type="text/javascript">
  jQuery(function($){
    $("#enderecoCep").mask("99999-999");
    $("#cpf").mask("999.999.999-99");
    $("#telefoneCasa").mask("(99) 9999-9999");
    $("#telefoneCelular").mask("(99) 9999-9999");
  });

  function setTextoNosCampos(nome, cpf, telCasa, telCelular, login, senha, permissoes, estado, cidade, bairro, cep, rua, numero, complemento) {
      document.getElementById('nome').value = nome;
      document.getElementById('cpf').value = cpf;
      document.getElementById('telefoneCasa').value = telCasa;
      document.getElementById('telefoneCelular').value = telCelular;
      document.getElementById('usuario').value = login;
      document.getElementById('senha').value = senha;
      document.getElementById('sel2').value = JSON.parse(permissoes);
      document.getElementById('enderecoEstado').value = estado;
      document.getElementById('enderecoCidade').value = cidade;
      document.getElementById('enderecoBairro').value = bairro;
      document.getElementById('enderecoCep').value = cep;
      document.getElementById('enderecoRua').value = rua;
      document.getElementById('numeroEndereco').value = numero;
      document.getElementById('enderecoComplemento').value = complemento;
    }
</script>
<?php
    $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');

      if(isset($_GET["id"]) && trim($_GET["id"]) ) {
        $resultFunc = mysqli_query($con,'SELECT * FROM atendente WHERE cd_atendente = '.$_GET["id"]);
        $row = mysqli_fetch_array($resultFunc);

      if(count($row) > 0) {
        $resultPermissao = mysqli_query($con,'SELECT cd_permissao FROM atendente_permissao WHERE cd_atendente = '. $_GET["id"]);
        $permissoes = '[';
        while($rowPer = mysqli_fetch_array($resultPermissao)) {
            if($permissoes != '['){
                $permissoes .= ',';
            }

            $permissoes .= $rowPer['cd_permissao'];
        }
        $permissoes .= ']';
      }

      if(count($row) > 0) {
        $resultEndereco = mysqli_query($con,'SELECT * FROM endereco WHERE cd_endereco = '. $row['cd_endereco']);
        $rowEnd = mysqli_fetch_array($resultEndereco);

        echo '<script type="text/javascript">'
           , 'setTextoNosCampos("'. $row['nm_atendente'] .'",
                                "'. $row['nr_cpf'] .'",
                                "'. $row['nr_telefone_casa'] .'",
                                "'. $row['nr_telefone_celular'] .'",
                                "'. $row['ds_login'] .'",
                                "'. $row['ds_senha'] .'",
                                "'. $permissoes .'",
                                "'. $rowEnd['ds_estado'] .'",
                                "'. $rowEnd['ds_cidade'] .'",
                                "'. $rowEnd['ds_bairro'] .'",
                                "'. $rowEnd['nr_cep'] .'",
                                "'. $rowEnd['ds_rua'] .'",
                                "'. $rowEnd['nr_complemento'] .'",
                                "'. $rowEnd['ds_complemento'] .'");'
            , '</script>';

      }
      mysqli_close($con);

    }

	require_once('footer.php');
?>
