<?php

	///////////////////////////////
	///			CENTRAL 		///
	///////////////////////////////
	function postPersist(){

		if($_SESSION['permissaoCadastrar'] == false || $_SESSION['permissaoCadastrar'] < 1) {
			$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
			header("Location: cadastro_categoria");
			exit;
		}

		$con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
	    if (mysqli_connect_errno()){
	      $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
	      mysqli_close($con);
	      exit;
	    }

		if(empty($_POST['descricao'])){
			$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Favor informar os dados corretamente!</div>');
			mysqli_close($con);
			exit;
		}

		if(isset($_GET["id"]) && trim($_GET["id"]) ) {
			//update
			$query = "UPDATE categoria SET nm_categoria = '" . $_POST['descricao'] . "' WHERE cd_categoria = ". $_GET["id"] .";";
			$msg = 'Alterado';
		} else {
			//insert
			$query = "INSERT INTO categoria(nm_categoria) VALUES ('" . $_POST['descricao'] . "');";
			$msg = 'Cadastrado';
		}

		if (mysqli_query($con, $query)){
			$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-success" role="alert">'. $msg . ' com sucesso!</div>');
			header("Location: produto");
			mysqli_query($con,"commit;");
			mysqli_close($con);
			exit;
		} else {
			mysqli_query($con,"rollback;");
			mysqli_close($con);
			$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Ocorreu um erro ao salvar. Tente novamente!</div>');
		}


	}

?>
