drop table tela_permissao;
drop table atendente_permissao;
drop table tela;
drop table permissao;
drop table item_aluguel;
drop table produto;
drop table categoria;
drop table aluguel;
drop table atendente;
drop table cliente;
drop table endereco;

CREATE TABLE tela (
    cd_tela INT AUTO_INCREMENT,
    nm_tela VARCHAR(50) NOT NULL,
    ds_arquivo VARCHAR(100) NOT NULL,
    fl_ativo SET('N', 'S')CHARACTER SET BINARY DEFAULT 'S',
    CONSTRAINT tela_pk PRIMARY KEY (cd_tela),
    CONSTRAINT tela_arquivo_uk UNIQUE (ds_arquivo),
    INDEX (nm_tela (50))
)  DEFAULT CHARSET=UTF8;

CREATE TABLE permissao (
    cd_permissao INT AUTO_INCREMENT,
    nm_permissao VARCHAR(50) NOT NULL,
    fl_ativo SET('N', 'S')CHARACTER SET BINARY DEFAULT 'S',
    CONSTRAINT permissao_pk PRIMARY KEY (cd_permissao),
    CONSTRAINT permissao_nome_uk UNIQUE (nm_permissao)
)  DEFAULT CHARSET=UTF8;

CREATE TABLE tela_permissao (
    cd_tela INT NOT NULL,
    cd_permissao INT NOT NULL,
    fl_visualizar SET('N', 'S')CHARACTER SET BINARY DEFAULT 'N',
    fl_cadastrar SET('N', 'S')CHARACTER SET BINARY DEFAULT 'N',
    CONSTRAINT tela_permissao_pk PRIMARY KEY (cd_tela,cd_permissao),
    CONSTRAINT permissao_tela_fk FOREIGN KEY (cd_tela)
        REFERENCES tela (cd_tela),
    CONSTRAINT tela_permissao_fk FOREIGN KEY (cd_permissao)
        REFERENCES permissao (cd_permissao)
)  DEFAULT CHARSET=UTF8;

CREATE TABLE endereco (
    cd_endereco INT AUTO_INCREMENT,
    ds_estado VARCHAR(25) NOT NULL,
    ds_cidade VARCHAR(50) NOT NULL,
    ds_bairro VARCHAR(50) NOT NULL,
    nr_cep VARCHAR(8) NOT NULL,
    ds_rua VARCHAR(50) NOT NULL,
    nr_complemento INT(6),
    ds_complemento VARCHAR(20),
    fl_ativo SET('N', 'S')CHARACTER SET BINARY DEFAULT 'S',
    CONSTRAINT endereco_pk PRIMARY KEY (cd_endereco),
    INDEX (nr_cep)
)  DEFAULT CHARSET=UTF8;

CREATE TABLE atendente (
    cd_atendente INT AUTO_INCREMENT,
    cd_endereco INT NOT NULL,
    nm_atendente VARCHAR(80) NOT NULL,
    nr_cpf VARCHAR(11) NOT NULL,
    nr_telefone_casa VARCHAR(10),
    nr_telefone_celular VARCHAR(10),
    ds_login VARCHAR(20) NOT NULL,
    ds_senha VARCHAR(100) NOT NULL,
    fl_ativo SET('N', 'S')CHARACTER SET BINARY DEFAULT 'S',
    CONSTRAINT atendente_pk PRIMARY KEY (cd_atendente),
    CONSTRAINT atendente_endereco_fk FOREIGN KEY (cd_endereco)
        REFERENCES endereco (cd_endereco),
    CONSTRAINT atendente_cpf_uk UNIQUE (nr_cpf),
    CONSTRAINT atendente_login_uk UNIQUE (ds_login),
    INDEX (nm_atendente)
)  DEFAULT CHARSET=UTF8;

CREATE TABLE atendente_permissao (
    cd_atendente INT NOT NULL,
    cd_permissao INT NOT NULL,
    ds_observacao TEXT(300),
    CONSTRAINT atendente_permissao_pk PRIMARY KEY (cd_atendente,cd_permissao),
    CONSTRAINT permissao_atendente_fk FOREIGN KEY (cd_atendente)
        REFERENCES atendente (cd_atendente),
    CONSTRAINT atendente_permissao_fk FOREIGN KEY (cd_permissao)
        REFERENCES permissao (cd_permissao)
)  DEFAULT CHARSET=UTF8;

CREATE TABLE cliente (
    cd_cliente INT AUTO_INCREMENT,
    cd_endereco INT(9) NOT NULL,
    nm_cliente VARCHAR(80) NOT NULL,
    dt_nascimento DATE NOT NULL,
    nr_rg VARCHAR(7) NOT NULL,
    nr_cpf VARCHAR(11) NOT NULL,
    nr_telefone_casa VARCHAR(10),
    nr_telefone_comercial VARCHAR(10),
    nr_telefone_celular VARCHAR(10),
    ds_observacao TEXT(300),
    fl_ativo SET('N', 'S')CHARACTER SET BINARY DEFAULT 'S',
    CONSTRAINT cliente_pk PRIMARY KEY (cd_cliente),
    CONSTRAINT cliente_endereco_fk FOREIGN KEY (cd_endereco)
        REFERENCES endereco (cd_endereco),
    CONSTRAINT cliente_rg_uk UNIQUE KEY (nr_rg),
    CONSTRAINT cliente_cpf_uk UNIQUE KEY (nr_cpf)
)  DEFAULT CHARSET=UTF8;

CREATE TABLE categoria (
    cd_categoria INT AUTO_INCREMENT,
    nm_categoria VARCHAR(50) NOT NULL,
    CONSTRAINT categoria_pk PRIMARY KEY (cd_categoria),
    CONSTRAINT categoria_nome_uk UNIQUE KEY (nm_categoria)
)  DEFAULT CHARSET=UTF8;

CREATE TABLE produto (
    cd_produto INT AUTO_INCREMENT,
    cd_categoria INT NOT NULL,
    nm_produto VARCHAR(80) NOT NULL,
    vl_compra DECIMAL(8 , 2 ) NOT NULL,
    vl_aluguel DECIMAL(8 , 2 ) NOT NULL,
    ds_tamanho CHAR(5) NOT NULL,
    dt_compra DATE NOT NULL,
    bl_foto BLOB,
    fl_ativo SET('N', 'S')CHARACTER SET BINARY DEFAULT 'S',
    CONSTRAINT produto_pk PRIMARY KEY (cd_produto),
    CONSTRAINT produto_categoria_fk FOREIGN KEY (cd_categoria)
        REFERENCES categoria (cd_categoria)
)  DEFAULT CHARSET=UTF8;

CREATE TABLE aluguel (
    cd_aluguel INT AUTO_INCREMENT,
    cd_cliente INT NOT NULL,
    cd_atendente INT NOT NULL,
    vl_pago DECIMAL(8 , 2 ) NOT NULL,
    vl_total DECIMAL(8 , 2 ) NOT NULL,
    dt_aluguel DATE NOT NULL,
    dt_prova DATE NOT NULL,
    dt_retirada DATE NOT NULL,
    dt_devolucao DATE NOT NULL,
    ds_observacao TEXT(300),
    fl_ativo SET('N', 'S')CHARACTER SET BINARY DEFAULT 'S',
    CONSTRAINT aluguel_pk PRIMARY KEY (cd_aluguel),
    CONSTRAINT aluguel_cliente_fk FOREIGN KEY (cd_cliente)
        REFERENCES cliente (cd_cliente),
    CONSTRAINT aluguel_atendente_fk FOREIGN KEY (cd_atendente)
        REFERENCES atendente (cd_atendente)
)  DEFAULT CHARSET=UTF8;

CREATE TABLE item_aluguel (
    cd_aluguel INT NOT NULL,
    cd_produto INT NOT NULL,
    ds_observacao TEXT(300),
    vl_item DECIMAL(8 , 2 ) NOT NULL,
    CONSTRAINT item_aluguel_pk PRIMARY KEY (cd_aluguel,cd_produto),
    CONSTRAINT produto_aluguel_fk FOREIGN KEY (cd_aluguel)
        REFERENCES aluguel (cd_aluguel),
    CONSTRAINT aluguel_produto_fk FOREIGN KEY (cd_produto)
        REFERENCES produto (cd_produto)
)  DEFAULT CHARSET=UTF8;

CREATE OR REPLACE VIEW produto_alugado AS
    SELECT 
        p.cd_produto,
        p.nm_produto,
        a.nm_atendente,
        c.cd_cliente,
        c.nm_cliente,
        al.cd_aluguel,
        al.vl_pago,
        al.vl_total,
        al.dt_prova,
        al.dt_retirada,
        al.dt_devolucao
    FROM
        cliente AS c,
        atendente AS a,
        produto AS p,
        aluguel AS al,
        item_aluguel AS ia
    WHERE
        al.cd_atendente = a.cd_atendente
            AND al.cd_cliente = c.cd_cliente
            AND al.cd_aluguel = ia.cd_aluguel
            AND p.cd_produto = ia.cd_produto;
            
CREATE OR REPLACE VIEW produto_vendido AS
    SELECT 
        p.cd_produto, p.nm_produto, COUNT(1)
    FROM
        produto AS p,
        item_aluguel AS ia
    WHERE
        p.cd_produto = ia.cd_produto
    GROUP BY cd_produto , nm_produto
    ORDER BY COUNT(1) DESC;
    
DELIMITER $$
CREATE OR REPLACE PROCEDURE verifica_alocacao (IN p_cd_aluguel INT,IN p_cd_produto INT, OUT p_fl_alugado BOOL)
BEGIN
    DECLARE alugado INT;
    DECLARE data_retirada DATE;
    DECLARE data_devolucao DATE;
    
SELECT 
    dt_retirada, dt_devolucao
INTO data_retirada , data_devolucao FROM
    aluguel
WHERE
    cd_aluguel = p_cd_aluguel;
    
SELECT 
    COUNT(1)
INTO alugado FROM
    produto_alugado
WHERE
    cd_aluguel <> p_cd_aluguel
        AND cd_produto = p_cd_produto
        AND ((dt_retirada >= data_retirada
        AND dt_devolucao <= data_devolucao)
        OR (dt_retirada >= data_retirada
        AND dt_devolucao <= data_devolucao));
    
		SET p_fl_alugado = (alugado > 0);
END $$
DELIMITER ;
  
DELIMITER $$
CREATE OR REPLACE TRIGGER trg_ibf_item_aluguel BEFORE INSERT ON item_aluguel
  FOR EACH ROW
  BEGIN
    DECLARE alugado BOOL DEFAULT FALSE;
    
    CALL verifica_alocacao(NEW.cd_aluguel, NEW.cd_produto, alugado);
    
    IF alugado THEN
		SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'Produto já alugado neste dia!';
    end if;
  END $$
  DELIMITER ;
  
DELIMITER $$
CREATE OR REPLACE TRIGGER trg_ubf_item_aluguel BEFORE UPDATE ON item_aluguel
  FOR EACH ROW
  BEGIN
    DECLARE alugado BOOL DEFAULT FALSE;
    
    CALL verifica_alocacao(NEW.cd_aluguel, NEW.cd_produto, alugado);
    
    IF alugado THEN
		SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'Produto já alugado neste dia!';
    END IF;
  END $$
  DELIMITER ;