<?php

	///////////////////////////////
	///			CENTRAL 		///
	///////////////////////////////
function postPersist(){

	if($_SESSION['permissaoCadastrar'] == false || $_SESSION['permissaoCadastrar'] < 1) {
	  $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
	  header("Location: cadastro_cliente");
	  exit;
	}

	$con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
    if (mysqli_connect_errno()) {
      	$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
      	mysqli_close($con);
      	exit;
    }

	if(empty($_POST['nome'])) {
		$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Favor informar os dados corretamente!</div>');
		mysqli_close($con);
		exit;
	}

	if(isset($_GET["id"]) && trim($_GET["id"]) ) {
		//UPDATE

		$queryCliente = "UPDATE cliente SET
			nm_cliente = '" . $_POST['nome'] . "',
			dt_nascimento = '" . $_POST['dtNascimento'] . "',
			nr_rg = '" . str_replace(array(".","-"),"",$_POST['rg']) . "',
			nr_cpf = '" . str_replace(array(".","-"),"",$_POST['cpf']) . "',
			nr_telefone_casa = '" . str_replace(array("(",")"," ","-"),"",$_POST['telefoneCasa']) . "',
			nr_telefone_celular = '" . str_replace(array("(",")"," ","-"),"",$_POST['telefoneCelular']) . "',
			nr_telefone_comercial = '" . str_replace(array("(",")"," ","-"),"",$_POST['telefoneComercial']) . "',
			ds_observacao = '" . $_POST['observacao'] . "'
			WHERE cd_cliente = ". $_GET["id"] .";";

		$queryEndereco = "UPDATE endereco SET
			ds_estado = '" . $_POST['enderecoEstado'] . "',
			ds_cidade = '" . $_POST['enderecoCidade'] . "',
			ds_bairro = '" . $_POST['enderecoBairro'] . "',
			nr_cep = '" . str_replace("-","",$_POST['enderecoCep']) . "',
			nr_complemento = " . $_POST['numeroEndereco'] . ",
			ds_complemento = '" . $_POST['enderecoComplemento'] . "'
			WHERE cd_endereco = ". $_POST["cd_endereco"] .";";

			if (mysqli_query($con, $queryCliente)){

				if(mysqli_query($con, $queryEndereco)) {
					$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-success" role="alert">Alterado com sucesso!</div>');
				  header("Location: cliente");
				  mysqli_close($con);
				  exit;
				} else {
					$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">'. var_dump($_POST) . '</div>');
					mysqli_close($con);

				}

				// $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">'. mysqli_error($con) . '</div>');

			} else {
				$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Ocorreu um erro ao salvar. Tente novamente!</div>');
				mysqli_close($con);

			}





	} else {
		//INSERT
		mysqli_query($con, "START TRANSACTION");
		if (mysqli_query($con,"INSERT INTO endereco(ds_estado, ds_cidade, ds_bairro, nr_cep, ds_rua, nr_complemento, ds_complemento)
									 VALUES ('" . $_POST['enderecoEstado'] . "', '" . $_POST['enderecoCidade'] . "', '" . $_POST['enderecoBairro'] . "', '" . str_replace("-","",$_POST['enderecoCep']) . "', '" . $_POST['enderecoRua'] . "', '" . $_POST['numeroEndereco'] . "', '" . $_POST['enderecoComplemento'] . "');")){
			$asd = "INSERT INTO cliente(cd_endereco, nm_cliente, dt_nascimento, nr_rg, nr_cpf, nr_telefone_casa, nr_telefone_comercial, nr_telefone_celular, ds_observacao)
									VALUES (" . mysqli_insert_id($con) . ", '" . $_POST['nome'] . "', '" . $_POST['dtNascimento'] . "', '" .
									str_replace(array(".","-"),"",$_POST['rg']) . "', '" .
									str_replace(array(".","-"),"",$_POST['cpf']) . "', '" .
									str_replace(array("(",")"," ","-"),"",$_POST['telefoneCasa']) . "', '" .
									str_replace(array("(",")"," ","-"),"",$_POST['telefoneComercial']) . "', '" .
									str_replace(array("(",")"," ","-"),"",$_POST['telefoneCelular']) . "', '" . $_POST['observacao'] . "');";
			if (mysqli_query($con,$asd) ){
				mysqli_query($con, "COMMIT");
				$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-success" role="alert">Salvo com sucesso!</div>');
				mysqli_close($con);
				header("Location: cliente");
				exit;
			}else {
				$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao gravar dados: ' . mysqli_error($con) . '</div>';
				mysqli_query($con, "ROLLBACK");
			}

		}else {
			$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao gravar dados: ' . mysqli_error($con) . '</div>';
			mysqli_query($con, "ROLLBACK");
		}

		mysqli_close($con);
	}
}
?>
