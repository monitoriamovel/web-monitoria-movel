<?php
    require_once('permissaoController.php');

    $nome_pag = "Cadastro de Permissao";
	require_once('header.php');

  $permissaoVer = false;
  $_SESSION['permissaoCadastrar'] = false;

  foreach ($_SESSION['Permissoes'] as $key => $value) {
    if(!is_array($value)) {
        if(stristr($nome_pag, $value)) {
          $arrayPerm = $_SESSION['Permissoes'][$value+1];

          if($arrayPerm['visualizar'] == "S") {
            $permissaoVer = true;
          }

          if($arrayPerm['cadastrar'] == "S") {
            $_SESSION['permissaoCadastrar'] = true;
          }
          break;
        }
    }
  }

  if(!$permissaoVer) {
    $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
    header("Location: atendente"); exit;
  }
?>

<div class="container">
	<div class="modal fade" id="modalTela" role="dialog">
    	<div class="modal-dialog">
      		<div class="modal-content">
        		<div class="modal-header">
          			<button type="button" class="close" data-dismiss="modal">&times;</button>
          			<h4 class="modal-title">Selecione a Tela</h4>
        		</div>
        		<table id="tabelaProdutos" class="table table-hover">
          			<thead>
            			<tr>
              				<th>Nome</th>
              				<th>Arquivo</th>
              				<th>Ações</th>
            			</tr>
          			</thead>
	          		<tbody>
	          			<?php
	          				$con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
      						    if (mysqli_connect_errno()) {
      						      $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
      						      excepitonExit();
      						    }
    				        	$resultPer = mysqli_query($con,"SELECT t.cd_tela, t.nm_tela, t.ds_arquivo
    				                                            FROM tela t");
    				          	while($row = mysqli_fetch_array($resultPer)) {
    				            	echo '	<tr>
    				                	  		<td>' . $row['nm_tela'] . '</td>
    				                   			<td>' . $row['ds_arquivo'] . '</td>
    				                   			<td>
    				                     			<button class="btn btn-info btn-sm" onclick="selecionaTela(' . $row['cd_tela'] . ', \'' . $row['nm_tela'] . '\')"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
    				                   			</td>
    				                 		</tr>';
    				          	}
    				          	mysqli_close($con);
				        ?>
	          		</tbody>
        		</table>
        		<div class="modal-footer">
          			<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        		</div>
      		</div>
    	</div>
  	</div>

	<form class="form-horizontal" onsubmit="organizaTelas()" role="form" action="" method="post">
        <fieldset>
		    <div class="form-group">
			    <div class="col-xs-5">
              <label for="ex1">Descrição</label>
              <input id="descricao" class="form-control" autofocus="" required="" type="text" placeholder="Descrição" name="descricao" >
          </div>
          <div class="col-xs-5" style="display: none;">
              <label>Objetos Selecionados</label>
              <input type="text" name="telasSelecionadas" class="form-control" id="telasSelecionadas">
          </div>
		    </div>

		    <h3>Acessos</h3>
          	<div class="form-group">
              <div class="col-xs-1" style="display: none;">
                <label>Código</label>
                  <input type="text" class="form-control" id="codigoTela">
              </div>
            	<div class="col-xs-4">
              		<label for="nomeTela">Tela</label>
              		<div class="input-group">
                		<input type="text" id="nomeTela" class="form-control" placeholder="Tela" disabled="true">
            			<div class="input-group-btn">
                			<span class="input-group-btn">
                  				<button class="btn btn-default" type="button" data-toggle="modal" data-target="#modalTela"><i class="fa fa-search"></i></button>
                			</span>
            			</div>
              		</div>
            	</div>
            	<div class="col-xs-1">
              		<label for="visualizarTela">Visualizar</label>
              		<span class="form-control" style="display: table-cell; position: relative; left: 40%;">
						<input type="checkbox" id="visualizarTela">
					</span>
            	</div>
            	<div class="col-xs-1">
              		<label for="cadastrarTela">Cadastrar</label>
                	<span class="form-control" style="display: table-cell; position: relative; left: 40%;">
						<input type="checkbox" id="cadastrarTela">
					</span>
            	</div>

            	<div class="col-xs-2">
            		<label>&nbsp;</label>
              		<div onclick="adicionarClick()" class="btn btn-success btn-primary btn-block"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar</div>
            	</div>
            	<div id="tabpagerPermissao" class="col-xs-12" style="margin-top: 20px;">
            		<table id="tabelaPermissao" class="table table-hover">
               			<thead>
                 			<tr>
                   				<th>Tela</th>
                   				<th>Visualizar</th>
                   				<th>Cadastrar</th>
                   				<th>Ações</th>
                 			</tr>
               			</thead>
               			<tbody id="conteudoAcessosSelecionados">
                      <?php
                        if(isset($_GET["id"]) && trim($_GET["id"]) ) {
                          $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
                          if (mysqli_connect_errno()) {
                            $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
                            mysqli_close($con);
                            exit;
                          }

                          $resultPer = mysqli_query($con,"SELECT t.cd_tela, t.nm_tela, tp.fl_visualizar, tp.fl_cadastrar
                                                            FROM tela_permissao tp, tela t
                                                           WHERE tp.cd_tela = t.cd_tela
                                                             AND tp.cd_permissao = '" . $_GET["id"] . "';");
                          while($row = mysqli_fetch_array($resultPer)) {
                            echo '<tr codigoTela="' . $row['cd_tela'] . '">
                                    <td>' . $row['nm_tela'] . '</td>
                                    <td>' . (($row['fl_visualizar'] == 'S') ? 'Sim' : 'Não') . '</td>
                                    <td>' . (($row['fl_cadastrar'] == 'S') ? 'Sim' : 'Não') . '</td>
                                    <td><button class="btn btn-danger btn-sm" onclick="removeLinha(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                                  </tr>';
                          }
                        }
                      ?>
               			</tbody>
             		</table>
             		<div class="pager">
             			<span class="page-number clickable active">1</span>
             		</div>
           		</div>
          	</div>
			<button type="submit" class="btn btn-success">Cadastrar</button>
		</fieldset>
	</form>
</div>
<script type="text/javascript">
  function organizaTelas(){
    var arrayObj = '[';
    $('#conteudoAcessosSelecionados tr').each(function(){
      if(arrayObj != '[')
        arrayObj += ',';

      arrayObj += '{';
      arrayObj += '"codigoTela":"'+$(this).attr('codigoTela')+'",';
      arrayObj += '"indVisualizar":"'+ (($('#conteudoAcessosSelecionados tr').children()[1].innerText == 'Sim') ? 2 : 1) +'",';
      arrayObj += '"indCadastrar":"'+ (($('#conteudoAcessosSelecionados tr').children()[2].innerText == 'Sim') ? 2 : 1) +'"';
      arrayObj += '}';
    });
    arrayObj += ']';

    $('#telasSelecionadas').val(arrayObj.toString());
  }

  function setTextDescricao(descricao) {
    document.getElementById('descricao').value = descricao;
  }

	function adicionarClick() {
		if ($('#nomeTela').val() == '') {
			alert('Favor selecionar a tela!');
			return;
		}
		$('#conteudoAcessosSelecionados').append(''
			+'<tr codigoTela="' + $('#codigoTela').val() + '">'
				+'<td>' + $('#nomeTela').val() + '</td>'
				+'<td>' + (($('#visualizarTela').prop('checked') == true) ? 'Sim' : 'Não') + '</td>'
				+'<td>' + (($('#cadastrarTela').prop('checked') == true) ? 'Sim' : 'Não') + '</td>'
				+'<td><button class="btn btn-danger btn-sm" onclick="removeLinha(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></td>'
			+'</tr>');

		$("#nomeTela").val('');
  		$("#codigoTela").val('');
  		$('#visualizarTela').prop('checked', false);
		$('#cadastrarTela').prop('checked', false);


		$('#tabelaPermissao').trigger('repaginar');
  	}

  	function selecionaTela(cd, nome) {
  		$("#nomeTela").val(nome);
  		$("#codigoTela").val(cd);
  		$("#modalTela").modal("hide");
	}

	function removeLinha(linha) {
		$(linha).parent().parent().remove();
		$('#tabelaPermissao').trigger('repaginar');
	}

	var currentPage = 0;
	var numRows = 0;
	function repaginar(id){
		// Adaptado de:  http://gabrieleromanato.name/jquery-easy-table-pagination/
		$(id).each(function() {
      		currentPage = 0;
      		var numPerPage = 6;
      		var $table = $(this);
      		$table.bind('trocarPag', function() {
          		$table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
  			});
  			$table.bind('repaginar', function() {
  				var numRowsNew = $table.find('tbody tr').length;
  				var numPages = Math.ceil(numRowsNew / numPerPage);

  				if (numRows < numRowsNew && currentPage < numPages-1)
  					currentPage++;
  				else if (numRows > numRowsNew && currentPage > numPages-1)
  					currentPage--;

          		numRows = numRowsNew;

          		$table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();

	  			var $pager = $('<div class="pager"></div>');
	  			for (var page = 0; page < numPages; page++) {
	          		$('<span class="page-number"></span>').text(page + 1).bind('click', {
	          			newPage: page
	      			}, function(event) {
	              		currentPage = event.data['newPage'];
	          			$table.trigger('trocarPag');
	          			$(this).addClass('active').siblings().removeClass('active');
	      			}).appendTo($pager).addClass('clickable').attr('id', 'pager-acesso-'+page);
	  			}

	  			$('#tabpagerPermissao .pager').replaceWith($pager);
	  			$('#tabpagerPermissao .pager #pager-acesso-'+currentPage).addClass('active').siblings().removeClass('active');
	  			$table.trigger('trocarPag');
  			});
    	});
	}

	$(document).ready(function(){
		repaginar('#tabelaPermissao');
	});
</script>

<?php

  //iasset verifica se a variavel existe e se tem vai setar o texto no campo correto
  if(isset($_GET["id"]) && trim($_GET["id"]) ) {
    $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
    $resultPer = mysqli_query($con,'SELECT nm_permissao from permissao where cd_permissao = '.$_GET["id"]);
    $row = mysqli_fetch_array($resultPer);
    $resultStr = (string) $row[0];
    echo '<script type="text/javascript">'
       , 'setTextDescricao("'. $resultStr .'");'
       , '</script>';
    mysqli_close($con);

  }

	require_once('footer.php');
?>
