<?php

	///////////////////////////////
	///			CENTRAL 		///
	///////////////////////////////
	function postPersist(){

		if($_SESSION['permissaoCadastrar'] == false || $_SESSION['permissaoCadastrar'] < 1) {
		  $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
		  header("Location: cadastro_produto");
		  exit;
		}

		$con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
	    if (mysqli_connect_errno()) {
	      	$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
	      	mysqli_close($con);
	      	exit;
	    }

		if(empty($_POST['descricao']) OR empty($_POST['vlCompra']) OR empty($_POST['vlAluguel'])
			OR empty($_POST['dsTamanho']) OR empty($_POST['dtCompra']) OR empty($_POST['sltCategoria'])){
			$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Favor informar os dados corretamente!</div>');
			mysqli_close($con);
			exit;
		}

		if(isset($_GET["id"]) && trim($_GET["id"]) ) {
		  //update

			if(getimagesize($_FILES['image']['tmp_name'])) {
			    $image= addslashes($_FILES['image']['tmp_name']);
			    $name= addslashes($_FILES['image']['name']);
			    $image= file_get_contents($image);
			    $image= base64_encode($image);

			  $query = "UPDATE produto SET
				cd_categoria = " . $_POST['sltCategoria'] . ",
				nm_produto = '" . $_POST['descricao'] . "',
				vl_compra = " . $_POST['vlCompra'] . ",
				vl_aluguel = " . $_POST['vlAluguel'] . ",
				ds_tamanho = '" . $_POST['dsTamanho'] . "',
				dt_compra = '" . $_POST['dtCompra'] . "',
				bl_foto = '" . $image . "'
				WHERE cd_produto = " . $_GET["id"] .";";
			} else {
				$query = "UPDATE produto SET
				cd_categoria = " . $_POST['sltCategoria'] . ",
				nm_produto = '" . $_POST['descricao'] . "',
				vl_compra = " . $_POST['vlCompra'] . ",
				vl_aluguel = " . $_POST['vlAluguel'] . ",
				ds_tamanho = '" . $_POST['dsTamanho'] . "',
				dt_compra = '" . $_POST['dtCompra'] . "'
				WHERE cd_produto = " . $_GET["id"] .";";
			}

		  $msg = 'Alterado';
		} else {
		  //insert

			if(getimagesize($_FILES['image']['tmp_name'])) {
			    $image= addslashes($_FILES['image']['tmp_name']);
			    $name= addslashes($_FILES['image']['name']);
			    $image= file_get_contents($image);
			    $image= base64_encode($image);
		    } else {
		    	$image = 'null';
		    }

		  $query = "INSERT INTO produto(cd_categoria, nm_produto, vl_compra, vl_aluguel, ds_tamanho, dt_compra, bl_foto) VALUES
			(" . $_POST['sltCategoria'] . ",
			'" . $_POST['descricao'] . "',
			" . $_POST['vlCompra'] . ",
			" . $_POST['vlAluguel'] . ",
			'" . $_POST['dsTamanho'] . "',
			'" . $_POST['dtCompra'] . "',
			'" . $image . "'
			);";

		  $msg = 'Cadastrado';
		}

		if (mysqli_query($con, $query)){
		  $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-success" role="alert">'. $msg . ' com sucesso!</div>');
		  header("Location: produto");
		  mysqli_close($con);
		  exit;
		} else {
		  $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">'.mysqli_error($con).'</div>');
		  // $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Ocorreu um erro ao salvar. Tente novamente!</div>');
			mysqli_close($con);
		}



	}

?>
