<?php 
    $nome_pag = "Home";
	require_once('header.php');
?>

<div class="container">
	<div class="bs-glyphicons">
		<ul class="bs-glyphicons-list">
			<a href="aluguel">
				<li>
					<i class="fa fa-calendar" aria-hidden="true"></i>
					<span class="glyphicon-class">Gerenciar<br/>Aluguel</span>
				</li>
			</a>
			<a href="cliente">
				<li>
					<i class="fa fa-child" aria-hidden="true"></i>
					<span class="glyphicon-class">Gerenciar<br/>Cliente</span>
				</li>
			</a>
			<a href="atendente">
				<li>
					<i class="fa fa-user" aria-hidden="true"></i>
					<span class="glyphicon-class">Gerenciar<br/>Atendente</span>
				</li>
			</a>
			<a href="produto">
				<li>
					<i class="fa fa-cogs" aria-hidden="true"></i>
					<span class="glyphicon-class">Gerenciar<br/>Produto</span>
				</li>
			</a>
		</ul>
	</div>
</div>

<?php 
	require_once('footer.php');
?>