<?php
  $nome_pag = "Cliente";
	require_once('header.php');


  $permissaoVer = false;
  $_SESSION['permissaoCadastrar'] = false;

  foreach ($_SESSION['Permissoes'] as $key => $value) {
    if(!is_array($value)) {
        if(stristr($nome_pag, $value)) {
          $arrayPerm = $_SESSION['Permissoes'][$value+1];

          if($arrayPerm['visualizar'] == "S") {
            $permissaoVer = true;
          }

          break;
        }
    }
  }

  if(!$permissaoVer) {
    $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
    header("Location: home"); exit;
  }

?>

<div class="container">

  <div class="row">
    <div class="col-md-1 col-md-offset-10">
      <a href="cadastro_cliente" class="btn btn-success btn-primary btn-block btn-logo"><i class="fa fa-plus" aria-hidden="true"></i> Novo</a>
    </div>

  </div>

  <table id="tabelaClientes" class="table table-hover">
     <thead>
       <tr>
         <th>Nome</th>
         <th>CPF</th>
         <th>Telefone Residencial</th>
         <th>Telefone Comercial</th>
         <th>Telefone Celular</th>
         <th>Ações</th>
       </tr>
     </thead>
     <tbody>
        <?php
          $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
          if (mysqli_connect_errno()) {
            $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
            mysqli_close($con);
            exit;
          }
          $resultFun = mysqli_query($con,'SELECT nm_cliente, nr_cpf, nr_telefone_casa, nr_telefone_comercial, nr_telefone_celular, cd_cliente
                                            FROM cliente');
          while($row = mysqli_fetch_array($resultFun)) {
            echo '<tr>
                    <td>' . $row['nm_cliente'] . '</td>
                    <td>' . Mask('###.###.###-##', $row['nr_cpf']) . '</td>
                    <td>' . Mask(((strlen($row['nr_telefone_casa']) == 11) ? '(##) #####-####' : '(##) ####-####'), $row['nr_telefone_casa']) . '</td>
                    <td>' . Mask(((strlen($row['nr_telefone_comercial']) == 11) ? '(##) #####-####' : '(##) ####-####'), $row['nr_telefone_comercial']) . '</td>
                    <td>' . Mask(((strlen($row['nr_telefone_celular']) == 11) ? '(##) #####-####' : '(##) ####-####'), $row['nr_telefone_celular']) . '</td>
                    <td>
                      <button class="btn btn-warning btn-sm" onclick="editarCliente('. $row['cd_cliente'] . ')"><i class="fa fa-pencil" aria-hidden="true"></i></button>

                    </td>
                  </tr>';
          }

          mysqli_close($con);
        ?>
     </tbody>
   </table>

</div>
<script type="text/javascript">
function editarCliente(cd_cliente) {
  location.href = './cadastro_cliente?id=' + cd_cliente;
}

function removerCelula(cel) {
 var p=cel.parentNode.parentNode;
     p.parentNode.removeChild(p);
     $('#tabelaClientes').trigger('repaginate');
}

  $(document).ready(function(){
    desenhaTodasTabelas();
  });

  function desenhaTodasTabelas() {
    desenhaPaginacao('#tabelaClientes');
  }

  function desenhaPaginacao(idTabela) {
    // Adaptado de:  http://gabrieleromanato.name/jquery-easy-table-pagination/
    $(idTabela).each(function() {
      var currentPage = 0;
      var numPerPage = 6;
      var $table = $(this);
      $table.bind('repaginate', function() {
          $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
      });
      $table.trigger('repaginate');
      var numRows = $table.find('tbody tr').length;
      var numPages = Math.ceil(numRows / numPerPage);
      var $pager = $('<div class="pager"></div>');
      for (var page = 0; page < numPages; page++) {
          $('<span class="page-number"></span>').text(page + 1).bind('click', {
              newPage: page
          }, function(event) {
              currentPage = event.data['newPage'];
              $table.trigger('repaginate');
              $(this).addClass('active').siblings().removeClass('active');
          }).appendTo($pager).addClass('clickable');
      }
      $pager.insertAfter($table).find('span.page-number:first').addClass('active');
    });
  }
</script>
<?php
	require_once('footer.php');
?>
