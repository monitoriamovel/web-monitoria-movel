<?php

  $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
  if (mysqli_connect_errno()) {
    $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
    mysqli_close($con);
    exit;
  }

  $dataSet = array();
  $resultPer = mysqli_query($con,"SELECT * FROM faturamento_mensal");
  while($row = mysqli_fetch_array($resultPer)) {
    array_push($dataSet, array($row['mes'], floatval($row['Valor'])) );
  }
  array_push($dataSet, array("Mes", "Valor"));
  $dataSet = array_reverse($dataSet);

  mysqli_close($con);
  echo json_encode($dataSet);
?>
