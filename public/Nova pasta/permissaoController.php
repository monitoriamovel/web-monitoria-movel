<?php

	///////////////////////////////
	///			CENTRAL 		///
	///////////////////////////////
	function postPersist(){

		if($_SESSION['permissaoCadastrar'] == false || $_SESSION['permissaoCadastrar'] < 1) {
		  $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
		  header("Location: cadastro_permissao");
		  exit;
		}

		$con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
	    if (mysqli_connect_errno()) {
	      $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
	      mysqli_close($con);
	      exit;
	    }

		if(empty($_POST['descricao'])){
			$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Favor informar os dados corretamente!</div>');
			mysqli_close($con);
			exit;
		}

		$telas = json_decode($_POST['telasSelecionadas']);

    	if(isset($_GET["id"]) && trim($_GET["id"]) ) {
		  	if (mysqli_query($con,"UPDATE permissao SET nm_permissao = '" . $_POST["descricao"] . "' WHERE cd_permissao = " . $_GET["id"] . ";") ){

		  		if (mysqli_query($con,"DELETE FROM tela_permissao WHERE cd_permissao = " . $_GET["id"] . ";") ){

					foreach ( $telas as $t ) {
						if (!mysqli_query($con,"INSERT INTO tela_permissao(cd_tela,cd_permissao,fl_visualizar,fl_cadastrar)
											VALUES (" . $t->codigoTela . "," . $_GET["id"] . "," . $t->indVisualizar . "," . $t->indCadastrar . ");") ){
							$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao gravar dados: ' . mysqli_error($con) . '</div>';
							mysqli_query($con,"rollback;");
						}
			    	}

			    	if (!isset($_SESSION['alertMessage'])){
						$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-success" role="alert">Salvo com sucesso!</div>');
						mysqli_query($con,"commit;");
						header("Location: atendente");
						exit;
					}
				} else {
					$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao alterar os dados: ' . mysqli_error($con) . '</div>';
					mysqli_query($con,"rollback;");
				}

			} else {
				$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao alterar os dados: ' . mysqli_error($con) . '</div>';
				mysqli_query($con,"rollback;");
			}
		} else {
			if (mysqli_query($con,"INSERT INTO permissao(nm_permissao)
										VALUES ('" . $_POST['descricao'] . "');") ){

				$idPermissao = mysqli_insert_id($con);

				foreach ( $telas as $t ) {
					if (!mysqli_query($con,"INSERT INTO tela_permissao(cd_tela,cd_permissao,fl_visualizar,fl_cadastrar)
										VALUES (" . $t->codigoTela . "," . $idPermissao . "," . $t->indVisualizar . "," . $t->indCadastrar . ");") ){
						$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao gravar dados: ' . mysqli_error($con) . '</div>';
						mysqli_query($con,"rollback;");
					}
		    	}
		    	if (!isset($_SESSION['alertMessage'])){
					$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-success" role="alert">Salvo com sucesso!</div>');
					mysqli_query($con,"commit;");
					mysqli_close($con);
					header("Location: atendente");
					exit;
				}
			}else{
				$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao gravar dados: ' . mysqli_error($con) . '</div>';
				mysqli_query($con,"rollback;");
			}
		}

		mysqli_close($con);
	}

	function getPermissao(){

	}

?>
