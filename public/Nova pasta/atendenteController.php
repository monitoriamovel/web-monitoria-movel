<?php

	///////////////////////////////
	///			CENTRAL 		///
	///////////////////////////////
	function postPersist(){

		if($_SESSION['permissaoCadastrar'] == false || $_SESSION['permissaoCadastrar'] < 1) {
		  $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">'.$_SESSION['permissaoCadastrar'].'</div>');
		  header("Location: cadastro_atendente");
		  exit;
		}


		$con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
	    if (mysqli_connect_errno()) {
	      	$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
	      	mysqli_close($con);
	      	exit;
	    }

		if(empty($_POST['nome'])) {
			$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Favor informar os dados corretamente!</div>');
			mysqli_close($con);
			exit;
		}

		if(isset($_GET["id"]) && trim($_GET["id"]) ) {
			if (mysqli_query($con,"UPDATE endereco SET ds_estado = '" . $_POST['enderecoEstado'] . "', ds_cidade = '" . $_POST['enderecoCidade'] . "', ds_bairro = '" . $_POST['enderecoBairro'] . "', nr_cep = '" . str_replace("-","",$_POST['enderecoCep']) . "', ds_rua = '" . $_POST['enderecoRua'] . "', nr_complemento = '" . $_POST['numeroEndereco'] . "', ds_complemento = '" . $_POST['enderecoComplemento'] . "' WHERE cd_endereco = (SELECT cd_endereco FROM atendente WHERE cd_atendente = " . $_GET["id"] . ");")){

				if (mysqli_query($con,"UPDATE atendente SET nm_atendente = '" . $_POST['nome'] . "', nr_cpf = '" . str_replace(array(".","-"),"",$_POST['cpf']) . "', nr_telefone_casa = '" . str_replace(array("(",")"," ","-"),"",$_POST['telefoneCasa']) . "', nr_telefone_celular = '" . str_replace(array("(",")"," ","-"),"",$_POST['telefoneCelular']) . "', ds_login = '" . $_POST['usuario'] . "', ds_senha = '" . $_POST['senha'] . "' WHERE cd_atendente = " . $_GET["id"] . ";")){

					if (mysqli_query($con,"DELETE FROM atendente_permissao WHERE cd_atendente = " . $_GET["id"] . ";") ) {

						foreach ($_POST['permissoes'] as $selectedOption){
							if (!mysqli_query($con,"INSERT INTO atendente_permissao(cd_atendente, cd_permissao)
														 VALUES (" . $_GET["id"] . ", " . $selectedOption . ");") ){
								$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao gravar dados: ' . mysqli_error($con) . '</div>';
								mysqli_query($con,"rollback;");
							}
						}

						if (!isset($_SESSION['alertMessage'])){
							$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-success" role="alert">Salvo com sucesso!</div>');
							mysqli_query($con,"commit;");
							mysqli_close($con);
							header("Location: atendente");
							exit;
						}

					} else {
						$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao gravar dados: ' . mysqli_error($con) . '</div>';
						mysqli_query($con,"rollback;");
					}

				} else {
					$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao gravar dados: ' . mysqli_error($con) . '</div>';
					mysqli_query($con,"rollback;");
				}

			} else {
				$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao gravar dados: ' . mysqli_error($con) . '</div>';
				mysqli_query($con,"rollback;");
			}

		} else {

			if (mysqli_query($con,"INSERT INTO endereco(ds_estado, ds_cidade, ds_bairro, nr_cep, ds_rua, nr_complemento, ds_complemento)
								   VALUES ('" . $_POST['enderecoEstado'] . "', '" . $_POST['enderecoCidade'] . "', '" . $_POST['enderecoBairro'] . "', '" . str_replace("-","",$_POST['enderecoCep']) . "', '" . $_POST['enderecoRua'] . "', '" . $_POST['numeroEndereco'] . "', '" . $_POST['enderecoComplemento'] . "');")){

				if (mysqli_query($con,"INSERT INTO atendente(cd_endereco, nm_atendente, nr_cpf, nr_telefone_casa, nr_telefone_celular, ds_login, ds_senha)
									   VALUES (" . mysqli_insert_id($con) . ", '" . $_POST['nome'] . "', '" . str_replace(array(".","-"),"",$_POST['cpf']) . "', '" . str_replace(array("(",")"," ","-"),"",$_POST['telefoneCasa']) . "', '" . str_replace(array("(",")"," ","-"),"",$_POST['telefoneCelular']) . "', '" . $_POST['usuario'] . "', '" . $_POST['senha'] . "');") ){

					$cd_func = mysqli_insert_id($con);
					foreach ($_POST['permissoes'] as $selectedOption){
						if (!mysqli_query($con,"INSERT INTO atendente_permissao(cd_atendente, cd_permissao)
													 VALUES (" . $cd_func . ", " . $selectedOption . ");") )
							$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao gravar dados: ' . mysqli_error($con) . '</div>';
							mysqli_query($con,"rollback;");
					}

					if (!isset($_SESSION['alertMessage'])){
						$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-success" role="alert">Salvo com sucesso!</div>');
						mysqli_query($con,"commit;");
						mysqli_close($con);
						header("Location: atendente");
						exit;
					}
				} else {
					$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao gravar dados: ' . mysqli_error($con) . '</div>';
					mysqli_query($con,"rollback;");
				}
			} else {
				$_SESSION['alertMessage'] = '<div class="alert alert-danger" role="alert">Erro ao gravar dados: ' . mysqli_error($con) . '</div>';
				mysqli_query($con,"rollback;");
			}

		}

		mysqli_close($con);
	}

	function getPermissao(){

	}

?>
