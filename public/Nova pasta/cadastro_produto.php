<?php
	require_once('produtoController.php');

    $nome_pag = "Cadastro de Produto";
	require_once('header.php');


	$permissaoVer = false;
	$_SESSION['permissaoCadastrar'] = false;

	foreach ($_SESSION['Permissoes'] as $key => $value) {
	  if(!is_array($value)) {
	      if(stristr($nome_pag, $value)) {
	        $arrayPerm = $_SESSION['Permissoes'][$value+1];

	        if($arrayPerm['visualizar'] == "S") {
	          $permissaoVer = true;
	        }

	        if($arrayPerm['cadastrar'] == "S") {
	          $_SESSION['permissaoCadastrar'] = true;
	        }
	        break;
	      }
	  }
	}

	if(!$permissaoVer) {
		$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
		header("Location: produto"); exit;
	}
?>

	<style type="text/css" media="all">
#foto {
	margin: 2%;
}
</style>

	<div class="container">
		<form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
			<fieldset>
				<div class="form-group">
					<div class="col-xs-8">
						<label for="descricao">Descrição do Produto</label>
						<input id="descricao" required="" name="descricao" class="form-control" type="textarea" placeholder="Descrição">
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-2">
						<label for="vlCompra">Valor de Compra</label>
						<div class="input-group">
							<span class="input-group-addon">R$</span>
							<input id="vlCompra" required="" name="vlCompra" class="form-control" type="text">
							<span class="input-group-addon">,00</span>
						</div>
					</div>
					<div class="col-xs-2">
						<label for="vlAluguel">Valor de Aluguel</label>
						<div class="input-group">
							<span class="input-group-addon">R$</span>
							<input id="vlAluguel" required="" name="vlAluguel" class="form-control" type="text">
							<span class="input-group-addon">,00</span>
						</div>
					</div>
					<div class="col-xs-2">
						<label for="dsTamanho">Tamanho</label>
						<input id="dsTamanho" required="" name="dsTamanho" class="form-control" type="text" placeholder="Tamanho">
					</div>
					<div class="col-xs-2">
						<label for="dtCompra">Data de Compra</label>
						<input id="dtCompra" required="" name="dtCompra" class="form-control datepicker" type="date" placeholder="Data Compra">
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-4">
						<input class="form-control" type="file" onchange="readURL(this);"
							accept="image/*" name="image"/> <img id="foto" src="#"
							alt="Foto Produto" class="form-control"
							style="visibility: hidden;" />
					</div>

					<div class="col-xs-4">
						<select required="" name="sltCategoria" class="form-control">
							<?php
                                $con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
                                if (mysqli_connect_errno()) {
                                  	$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
                                  	mysqli_close($con);
	      							exit;
                                }
                                $resultPer = mysqli_query($con,"SELECT cd_categoria, nm_categoria
                                                                  FROM categoria");
                                while($row = mysqli_fetch_array($resultPer)) {
                                    echo '<option value="' . $row['cd_categoria'] . '">' . $row['nm_categoria'] . '</option>';
                                }

                                mysqli_close($con);
                            ?>
						</select>
					</div>
				</div>
				<button type="submit" class="btn btn-success">Salvar</button>
			</fieldset>
		</form>
	</div>
	<script type="text/javascript">

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('#foto').css({'visibility':'visible','width':'100%','height':'auto'}).attr('src',
							e.target.result);
				};

				reader.readAsDataURL(input.files[0]);
			}
		};


	function setTextoNosCampos(nome, vlCompra, vlAluguel, tamanho, dtCompra, foto, categoria) {
    document.getElementById('descricao').value = nome;
    document.getElementById('vlCompra').value = vlCompra;
		document.getElementById('vlAluguel').value = vlAluguel;
		document.getElementById('dsTamanho').value = tamanho;
		document.getElementById('dtCompra').value = dtCompra;

		document.getElementById('foto').setAttribute( 'src', 'data:image/png;base64,' + foto );
		$('#foto').css({'visibility':'visible','width':'100%','height':'auto'});
  }

	</script>


<?php

if(isset($_GET["id"]) && trim($_GET["id"]) ) {
	$con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
	$resultProd = mysqli_query($con,'SELECT * from produto where cd_produto = '.$_GET["id"]);
	$row = mysqli_fetch_array($resultProd);

	$resultCat = mysqli_query($con, 'SELECT * from categoria where cd_categoria = '. $row['cd_categoria']);
	$rowCat = mysqli_fetch_array($resultCat);


	echo '<script type="text/javascript">'
     , 'setTextoNosCampos("'. $row['nm_produto'] .'",
                          "'. intval($row['vl_compra']) .'",
                          "'. intval($row['vl_aluguel']) .'",
													"'. $row['ds_tamanho'] .'",
													"'. $row['dt_compra'] .'",
													"'. $row['bl_foto'] .'",
													"'. $rowCat['cd_categoria'] .'"
												);'
      , '</script>';

	mysqli_close($con);

}


	require_once('footer.php');
?>
