<?php
	$nome_pag = "Produto";
	require_once ('header.php');

	$permissaoVer = false;
  $_SESSION['permissaoCadastrar'] = false;

  foreach ($_SESSION['Permissoes'] as $key => $value) {
    if(!is_array($value)) {
        if(stristr($nome_pag, $value)) {
          $arrayPerm = $_SESSION['Permissoes'][$value+1];

          if($arrayPerm['visualizar'] == "S") {
            $permissaoVer = true;
          }

          break;
        }
    }
  }

  if(!$permissaoVer) {
    $_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Você não tem permissão!</div>');
    header("Location: home"); exit;
  }

?>

	<div class="container">
		<div class="row">
		    <div class="col-md-1 col-md-offset-10">
		      <a href="cadastro_produto" class="btn btn-success btn-primary btn-block" style="padding-right: 125px;"><i class="fa fa-plus" aria-hidden="true"></i> Novo Produto</a>
		      <a href="cadastro_categoria" class="btn btn-success btn-primary btn-block" style="padding-right: 125px;"><i class="fa fa-plus" aria-hidden="true"></i> Nova Categoria</a>
		    </div>
		</div>
	<!-- TABELA -->
	    <div class="form-group">
			<div class="col-xs-12">
				<h3>Lista de Produtos</h3>
				<table id="tabelaProdutos" class="table table-hover">
					<thead>
						<tr>
							<th>Descrição</th>
							<th>Categoria</th>
							<th>Tamanho</th>
							<th>Ações</th>
						</tr>
					</thead>
					<tbody>
						<?php
          					$con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
          					if (mysqli_connect_errno()) {
            					$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
            					mysqli_close($con);
            					exit;
          					}

				          	$resultPer = mysqli_query($con,"SELECT p.nm_produto, c.nm_categoria, p.ds_tamanho, p.cd_produto
                                            				  FROM produto p, categoria c
                                            				 WHERE p.cd_categoria = c.cd_categoria");
          					while($row = mysqli_fetch_array($resultPer)) {
	            				echo '	<tr>
	                   						<td>' . $row['nm_produto'] . '</td>
	                   						<td>' . $row['nm_categoria'] . '</td>
	                   						<td>' . $row['ds_tamanho'] . '</td>
	                   						<td>
																<button class="btn btn-warning btn-sm" onclick="editarProduto('. $row['cd_produto'] . ')"><i class="fa fa-pencil" aria-hidden="true"></i></button>
	                   						</td>
	                 					</tr>';
          					}
          					mysqli_close($con);
        				?>
					</tbody>
				</table>

				<h3>Categorias</h3>
			    <table id="tabelaCategoria" class="table table-hover">
			      	<thead>
				        <tr>
				          	<th scope="col">Descrição</th>
				        	<th scope="col">Ação</th>
				        </tr>
				    </thead>
				    <tbody>
				        <?php
				        	$con = mysqli_connect('localhost', 'root', '', 'trafnprog3');
                            if (mysqli_connect_errno()) {
                              	$_SESSION['alertMessage'] = utf8_decode('<div class="alert alert-danger" role="alert">Erro ao conectar a base de dados!</div>');
                              	mysqli_close($con);
      							exit;
                            }
				          	$resultCat = mysqli_query($con,"SELECT nm_categoria, cd_categoria
				                                              FROM categoria");
				          	while($row = mysqli_fetch_array($resultCat)) {
				            echo '<tr>
				                   	<td>' . $row['nm_categoria'] . '</td>
				                   	<td>
														<button class="btn btn-warning btn-sm" onclick="editarCategoria('. $row['cd_categoria'] . ')"><i class="fa fa-pencil" aria-hidden="true"></i></button>
				                   	</td>
				                  </tr>';
				          	}
				          	mysqli_close($con);
				        ?>
				    </tbody>
			   	</table>
	      	</div>
	    </div>
	</div>
	<script type="text/javascript">

	function editarCategoria(cd_categoria) {
		location.href = './cadastro_categoria?id=' + cd_categoria;

	}

	function editarProduto(cd_produto) {
		location.href = './cadastro_produto?id=' + cd_produto;

	}

	function removerCelula(cel) {
	 var p=cel.parentNode.parentNode;
	     p.parentNode.removeChild(p);
	     $('#tabelaProdutos').trigger('repaginate');
	}

	  $(document).ready(function(){
	    desenhaTodasTabelas();
	  });

	  function desenhaTodasTabelas() {
	    desenhaPaginacao('#tabelaProdutos');
	  }

	  function desenhaPaginacao(idTabela) {
	    // Adaptado de:  http://gabrieleromanato.name/jquery-easy-table-pagination/
	    $(idTabela).each(function() {
	      var currentPage = 0;
	      var numPerPage = 5;
	      var $table = $(this);
	      $table.bind('repaginate', function() {
	          $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
	      });
	      $table.trigger('repaginate');
	      var numRows = $table.find('tbody tr').length;
	      var numPages = Math.ceil(numRows / numPerPage);
	      var $pager = $('<div class="pager"></div>');
	      for (var page = 0; page < numPages; page++) {
	          $('<span class="page-number"></span>').text(page + 1).bind('click', {
	              newPage: page
	          }, function(event) {
	              currentPage = event.data['newPage'];
	              $table.trigger('repaginate');
	              $(this).addClass('active').siblings().removeClass('active');
	          }).appendTo($pager).addClass('clickable');
	      }
	      $pager.insertAfter($table).find('span.page-number:first').addClass('active');
	    });
	  }
	</script>
<?php
require_once ('footer.php');
?>
