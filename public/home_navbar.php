    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <ul class="nav navbar-nav">
          <li><img id="logoNav" src="src/img/logo_4.png" style="background-color: lightblue"></li>
          <li <?php if ($nome_pag == "Monitores") echo 'class="active"'; ?> ><a href="monitores">Monitores</a></li>
		  <li <?php if ($nome_pag == "Relatorios") echo 'class="active"'; ?> ><a href="relatorios">Relatórios</a></li>
		  <li <?php if ($nome_pag == "Info") echo 'class="active"'; ?> ><a href="configuracoes">Info</a></li>		  
        </ul>
        <ul class='nav navbar-nav navbar-right'>
          <li><a href='logout' id='deslogar'><i class='fa fa-sign-out'></i> Deslogar</a></li>
        </ul>
      </div>
    </nav>