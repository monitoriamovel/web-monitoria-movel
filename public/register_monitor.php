<?php
    $nome_pag = "Registro de Monitor";

	include 'header.php';
	
	use Parse\ParseObject;
	use Parse\ParseQuery;
	use Parse\ParseACL;
	use Parse\ParsePush;
	use Parse\ParseUser;
	use Parse\ParseInstallation;
	use Parse\ParseException;
	use Parse\ParseAnalytics;
	use Parse\ParseFile;
	use Parse\ParseCloud;
	use Parse\ParseClient;
		
	if(isset($_SESSION['user'])){
		header("location: home");
	}	
	
	if ($_SERVER ["REQUEST_METHOD"] == "POST") {
		try {
			$username = escape_all_specials ( $_POST ['username'] );
			$password = escape_all_specials ( md5($_POST ['password']) );
			$bool = false;

			$query = new ParseQuery("usuario");
			$query->equalTo("usuario",$username);

			$user = $query->first();
			if($user != null){
				//erro username ja existe
				echo "<script language='javascript'>alert('Nome de usuario ja existente!');</script>"; 
			} else {
				//nenhum usuario com esse username
				//salva novo usuario

				$user = new ParseObject("usuario");
				$user->set("username", $username);
				$user->set("password", $password);

				//seta outros campos
				// other fields can be set just like with ParseObject
				$user->set("phone", "415-392-0202");

				//save new user
				
				//$user = $query->get("lFBxjzxfxh"); //id on mongo db
				//echo $user->get("usuario") . ', ' 
				//. $user->get('nome') . ', ' 
				//. $user->get('senha') . ', ' 
				//. $user->getCreatedAt();
				//echo 'Success!';
			
	/*			$query = new ParseQuery("usuario");
				$query->equalTo("usuario",$username);
				$query->equalTo("senha",$password);

				$user = $query->first();
			
				if($user != null){ //exist user 
						echo nl2br("\n\n" . $user->get("usuario") . ', ' 
					  . $user->get('nome') . ', ' 
					  . $user->get('senha'));
				}
				*/
				echo "<script language='javascript'>alert('Registrado com sucesso!');</script>"; 
				//redireciona para login
				echo "<script language='javascript'>window.location.assign('login');</script>"; // redirects to login page	
				//redirect pelo header nao mostra popups
				header('location: index');				
			}
		} catch (ParseException $ex) {
			// The login failed. Check error to see why.
			echo "Error: " . $ex->getCode() . " " . $ex->getMessage();
		}
	}
?>
	<h2>Registration Page</h2>
	<a href="login">Click here to go back!</a>
	<form action="register" method="POST">
		Enter Username: <input type="text" name="username" required="required" />
		<br /> Enter password: <input type="password" name="password"
			required="required" /> <br /> <input type="submit" value="Register" />
	</form>

<?php
	include 'footer.php';
?>