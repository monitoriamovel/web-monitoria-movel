<?php 
	$nome_pag = "Relatorios Usuarios";
	include 'header.php';

	if(!isset($_SESSION['user'])){
		header("location: index");
	} else {
		$user = $_SESSION['user'];
	}
	
	use Parse\ParseObject;
	use Parse\ParseQuery;
	use Parse\ParseACL;
	use Parse\ParsePush;
	use Parse\ParseUser;
	use Parse\ParseInstallation;
	use Parse\ParseException;
	use Parse\ParseAnalytics;
	use Parse\ParseFile;
	use Parse\ParseCloud;
	use Parse\ParseClient;
		
	try {
		$chart = new ParseObject("chart");

		if(isset($_GET['dtInicio']) && trim($_GET['dtInicio']) != '') {
			$chart->set("dtInicio",$_GET['dtInicio']);
		} else {
			if(date("m")-2 < 1){
				$mes = (date("m")-2+12); 
				$chart->set("dtInicio", date("Y")-1 . '-' . ($mes < 10 ? '0'.$mes : $mes) . '-' . date("d"));
				echo $mes . '<br/>' . $chart->get("dtInicio") . '<br/>';
			} else {
				$mes = (date("m")-2);
				$chart->set("dtInicio", date("Y") . '-' . ($mes < 10 ? '0'.$mes : $mes) . '-' . date("d"));
			}
		}
		if(isset($_GET['dtFim']) && trim($_GET['dtFim']) != '') {
			$chart->set("dtFim",$_GET['dtFim']);
		} else{
			$chart->set("dtFim",date("Y-m-d"));			
		}		
					
		$dtInicio = new DateTime($chart->get("dtInicio")); 
		$dtFim = new DateTime($chart->get("dtFim")); 

		if($dtInicio > $dtFim){
			$dt = $dtInicio;
			$dtInicio = $dtFim;
			$dtFim = $dt;
			
			$dt = $chart->get("dtInicio");
			$chart->set("dtInicio",$chart->get("dtFim"));
			$chart->set("","");
		}

		$dtInicio->setTime(0,0,0);
		$dtFim->setTime(23,59,59);
		

		
		$iniMes = $dtInicio->format('m')+0;
		$iniAno = $dtInicio->format('Y')+0;
		$endMes = $dtFim->format('m')+1;
		$endAno = $dtFim->format('Y')+0;

		if($endMes > 12){
			$endMes = 1;
			$endAno = $endAno+1;
		}
		
		$data[] = array();
		$qtd = 0;
		
		while(!($iniMes == $endMes && $iniAno == $endAno)){
			$query = new ParseQuery("usuario");
			$query->equalTo("verificado",0);
			
			$dtIniBusca = new DateTime(); 
			$dtIniBusca->setDate($iniAno, $iniMes, 1);
			$dtIniBusca->setTime(0,0,0);
			
			$endMonth = $iniMes+1;
			$endYear = $iniAno;
			if($endMonth > 12){
				$endMonth = 1;
				$endYear = $endYear + 1;
			}
			$dtFimBusca = new DateTime();
			$dtFimBusca->setDate($endYear, $endMonth, 1);
			$dtFimBusca->setTime(0,0,0);
			
			$query->greaterThanOrEqualTo('createdAt',$dtIniBusca);
			$query->lessThan('createdAt',$dtFimBusca);

			$resultPer = $query->find();
			
			$data[$qtd] = array($iniMes, $iniAno, count($resultPer));			
			$qtd = $qtd+1;
			
			$iniMes = $endMonth;
			$iniAno = $endYear;			
		}
		$chart->setArray("data",$data);
		$_SESSION['graficoBusca'] = $chart;		
		
		$size = count($_SESSION['graficoBusca']->get("data"));
		$chart = $_SESSION['graficoBusca']->get("data");
		
	} catch (ParseException $ex) {
		// The login failed. Check error to see why.
		echo "Error: " . $ex->getCode() . " " . $ex->getMessage();
	}
	
?>
	<!-- css local -->
	<style type="text/css" media="all">		
    </style>

	<div class="container">
		<h2>Usuarios</h2>
		<div class="form-group" style="margin-bottom: 0px">
			<div class="col-xs-12">
				<form class="form-horizontal" role="form" action="" method="GET" enctype="multipart/form-data">
					<fieldset>
						<div class="form-group">
						
							<div class="col-xs-2">
								<label for="dtInicio">Inicio</label>
								<input id="dtInicio" name="dtInicio" class="form-control datepicker" type="date"
								value="<?php echo $_SESSION['graficoBusca']->get("dtInicio");?>">
							</div>
							<div class="col-xs-2">
								<label for="dtFim">Fim</label>
								<input id="dtFim" name="dtFim" class="form-control datepicker" type="date" placeholder=""
								value="<?php echo $_SESSION['graficoBusca']->get("dtFim");?>">
							</div>							
						
						</div>	
						<button id="buscarGraph" type="submit" class="btn btn-success">Buscar</button>
					</fieldset>
				</form>
			</div>
		</div>
	
	    <div class="form-group">
			<div class="col-xs-12">
				<form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
					<fieldset>
						<div class="form-group">
							<div class="col-xs-12">
								<canvas id="graficoNovosUsuarios" width="800" height="350"></canvas>							
							</div>
						</div>						
					</fieldset>
				</form>			
			</div>			
		</div>
		
	</div>
	
	<script type="text/javascript">
		function getMes(num, ano){
			var mes = '';
			switch(num){
				case 1:
					mes = 'Jan';
					break;
				case 2:
					mes = 'Fev';
					break;
				case 3:
					mes = 'Mar';
					break;
				case 4:
					mes = 'Abr';
					break;
				case 5:
					mes = 'Mai';
					break;
				case 6:
					mes = 'Jun';
					break;
				case 7:
					mes = 'Jul';
					break;
				case 8:
					mes = 'Ago';
					break;
				case 9:
					mes = 'Set';
					break;
				case 10:
					mes = 'Out';
					break;
				case 11:
					mes = 'Nov';
					break;
				case 12:
				default:
					mes = 'Dez';
					break;
			}
			return mes + '/' + ano;
		};

	</script>
	
<!--		/* Script para graficos de busca*/	-->
	<script>	
		var ctx = document.getElementById("graficoNovosUsuarios").getContext('2d');
		var myChart = new Chart(ctx, {
			type: 'line',
			data: {
				labels: [
				<?php 
					$size = count($_SESSION['graficoBusca']->get("data"));
					$chart = $_SESSION['graficoBusca']->get("data");

					for($x=0;$x<$size;$x++){
						if($x == $size-1){
							echo 'getMes(' . $chart[$x][0] . ',' . $chart[$x][1] . ')';
						} else {
							echo 'getMes(' . $chart[$x][0] . ',' . $chart[$x][1] . '),';
						}
					}
				?>
				],
				datasets: [{
					label: 'Novos usuários',  
					data: [
					<?php 
						$size = count($_SESSION['graficoBusca']->get("data"));
						$chart = $_SESSION['graficoBusca']->get("data");

						for($x=0;$x<$size;$x++){
							if($x == $size-1){
								echo $chart[$x][2] . '';
							} else {
								echo $chart[$x][2] . ',';
							}
						}
					?>
					],
					backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(54, 162, 235, 0.2)',
						'rgba(255, 206, 86, 0.2)',
						'rgba(75, 192, 192, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(255, 159, 64, 0.2)'
					],
					borderColor: [
						'rgba(255,99,132,1)',
						'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)',
						'rgba(75, 192, 192, 1)',
						'rgba(153, 102, 255, 1)',	
						'rgba(255, 159, 64, 1)'
					],
					borderWidth: 2
				}]
			},
			options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Crescimento de Usuários'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mês'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Value'
                        }
                    }]
                }
            }
		});
	</script>
	

	
<?php
	include 'footer.php';
?>