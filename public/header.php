<?php
	require $_SERVER['DOCUMENT_ROOT'] . '/parseConfig.php';
	require_once $_SERVER['DOCUMENT_ROOT'] . '/utilities.php';
		
	use Parse\ParseObject;
	use Parse\ParseQuery;
	use Parse\ParseACL;
	use Parse\ParsePush;
	use Parse\ParseUser;
	use Parse\ParseInstallation;
	use Parse\ParseException;
	use Parse\ParseAnalytics;
	use Parse\ParseFile;
	use Parse\ParseCloud;
	use Parse\ParseClient;
?>


<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Monitoria Movel - <?php echo $nome_pag ?></title>

    <script src="src/js/jquery-3.2.1.min.js"></script>
    <script src="src/js/jquery.maskedinput.js"></script>
	<script src="src/js/chart/Chart.js"></script>
	<script src="src/js/chart/Chart.min.js"></script>	
	<script src="src/js/chart/Chart.bundle.js"></script>	
	<script src="src/js/chart/Chart.bundle.min.js"></script>

	<script src="src/js/html_to_excel/jquery.table2excel.js"></script>
	
    <link href="src/css/bootstrap.min.css" rel="stylesheet">
    <link href="src/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> 

    <link href="src/css/style.css" rel="stylesheet">

	<style type="text/css" media="all">
		.btn-success {
			background-color: #005FA4;
			border-color: #005FA4;
		}
		.btn-success:hover{
			background-color: #00549A;
		}
		.btn-success:active{
			background-color: #00549A;
		}
    </style>
	
  </head>
  <body>

	<?php
		if(isset($_SESSION['user'])){
			include 'home_navbar.php';
		}
	?>
  
    <div class="container" style="margin-bottom: 10px;">
      <div id="msg-retorno">
      <?php
        if (isset($_SESSION['alertMessage'])){
          echo utf8_encode($_SESSION['alertMessage']);
          unset($_SESSION['alertMessage']);
        }
      ?>
      </div>
    </div>
