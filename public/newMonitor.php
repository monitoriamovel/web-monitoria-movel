<?php
	session_start();

	require $_SERVER['DOCUMENT_ROOT'] . '/parseConfig.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/utilities.php';
		
	if(!isset($_SESSION['user'])){
		header("location: index");
	} else {
		$user = $_SESSION['user'];
	}
		
	use Parse\ParseObject;
	use Parse\ParseQuery;
	use Parse\ParseACL;
	use Parse\ParsePush;
	use Parse\ParseUser;
	use Parse\ParseInstallation;
	use Parse\ParseException;
	use Parse\ParseAnalytics;
	use Parse\ParseFile;
	use Parse\ParseCloud;
	use Parse\ParseClient;

	if ($_SERVER ["REQUEST_METHOD"] == "POST") {
		try {
			$_SESSION['monitor'] = 'new';
		
			header('location: monitores');
		} catch (ParseException $ex) {
			// The login failed. Check error to see why.
			echo "Error: " . $ex->getCode() . " " . $ex->getMessage();
		}
	}	
?>